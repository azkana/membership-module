<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberSkill extends Model
{
    protected $table = 'mbr_skills';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'name',
        'level',
        'member_id',
        'created_by',
        'updated_by'
    ];

    public function getMember()
    {
        return $this->belongsTo('Modules\Membership\Entities\Members', 'member_id');
    }
}
