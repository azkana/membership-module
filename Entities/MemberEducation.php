<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class MemberEducation extends Model
{
    use TraitsUuid;
    
    protected $table = 'mbr_education';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'degree',
        'institution',
        'majors',
        'start_year',
        'graduation',
        'on_progress',
        'grade',
        'kab_id',
        'prov_id',
        'note',
        'member_id',
        'created_by',
        'updated_by'
    ];

    public function getMember()
    {
        return $this->belongsTo('Modules\Membership\Entities\Members', 'member_id');
    }

    public static function getDegree()
    {
        $data = [
            'SMA' => 'SMA',
            'SMK' => 'SMK',
            'D3' => 'D3',
            'S1' => 'S1'
        ];
        return $data;
    }

    public function getProvinsi()
    {
        return $this->belongsTo('App\Models\Master\Provinsi', 'prov_id', 'code');
    }
}
