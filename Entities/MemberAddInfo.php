<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberAddInfo extends Model
{
    protected $table = 'mbr_add_info';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'mother',
        'father',
        'expected_salary',
        'expected_location',
        'other_info',
        'member_id',
        'created_by',
        'updated_by'
    ];

    public function getMember()
    {
        return $this->belongsTo('Modules\Membership\Entities\Members', 'member_id');
    }
}
