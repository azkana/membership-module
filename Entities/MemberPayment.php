<?php

namespace Modules\Membership\Entities;

use App\Models\Configs\Sequence;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class MemberPayment extends Model
{
    use TraitsUuid;

    protected $table = 'mbr_payments';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'number',
        'amount',
        'method',
        'status',
        'token',
        'payload',
        'payment_type',
        'va_number',
        'vendor_name',
        'biller_code',
        'bill_key',
        'order_id'
    ];

    public function getOrder()
    {
        return $this->belongsTo('Modules\Membership\Entities\MemberOrder', 'order_id', 'order_id');
    }

    public const PAYMENT_CHANNELS = [
        "credit_card", "cimb_clicks",
        "bca_klikbca", "bca_klikpay", "bri_epay", "echannel", "permata_va",
        "bca_va", "bni_va", "bri_va", "other_va", "gopay", "indomaret",
        "danamon_online", "akulaku", "shopeepay"
    ];

    public const EXPIRY_DURATION = 7;
    public const EXPIRY_UNIT = 'days';

    public const CHALLENGE = 'challenge';
    public const SUCCESS = 'success';
    public const SETTLEMENT = 'settlement';
    public const PENDING = 'pending';
    public const DENY = 'deny';
    public const EXPIRE = 'expire';
    public const CANCEL = 'cancel';

    public const PAYMENTCODE = 'PAY';

    public static function _generatePaymentNumber()
    {
        $getSequenceNumber = Sequence::_getNextVal('membership', 'member_payment', date('m'));
        $prefix = self::PAYMENTCODE;
        $del    = '/';
        $month  = date('m');
        $year   = date('Y');
        $increment = str_pad($getSequenceNumber, 4, '0', STR_PAD_LEFT);

        $paymentNumber = $year . $month . $increment . $del . $prefix . $del . monthToRoman($month) . $del . $year;
        return $paymentNumber;
    }
}
