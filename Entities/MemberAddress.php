<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class MemberAddress extends Model
{

    use TraitsUuid;

    protected $table = 'mbr_address';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'type',
        'name',
        'line1',
        'line2',
        'rt',
        'rw',
        'blok',
        'no',
        'kel_id',
        'kec_id',
        'kab_id',
        'prov_id',
        'zipcode',
        'phone',
        'member_id',
        'created_by',
        'updated_by'
    ];

    public function getMember()
    {
        return $this->belongsTo('Modules\Membership\Entities\Members', 'member_id');
    }

    public function getProvinsi()
    {
        return $this->belongsTo('App\Models\Master\Provinsi', 'prov_id', 'code');
    }

    public function getKabupaten()
    {
        return $this->belongsTo('App\Models\Master\Kabupaten', 'kab_id', 'code');
    }

    public function getKecamatan()
    {
        return $this->belongsTo('App\Models\Master\Kecamatan', 'kec_id', 'code');
    }

    public function getKelurahan()
    {
        return $this->belongsTo('App\Models\Master\Kelurahan', 'kel_id', 'code');
    }

    public function scopeKtpAddress($query)
    {
        return $query->where('type', 'ktp');
    }

    public function scopeDomAddress($query)
    {
        return $query->where('type', 'dom');
    }

    public function scopeEmrAddress($query)
    {
        return $query->where('type', 'emr');
    }

    public const ADDRESS_TYPE_KTP = 'ktp';
    public const ADDRESS_TYPE_DOM = 'dom';
    public const ADDRESS_TYPE_EMR = 'emr';

}
