<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class MemberType extends Model
{
    use TraitsUuid;

    protected $table = 'mbr_types';

    protected $fillable = [
        'code',
        'name',
        'description',
        'amount',
        'is_active',
        'created_by',
        'updated_by'
    ];

    public static function getMemberTypeForDropdownList($status = null)
    {
        if($status != null) {
            return self::where('is_active', $status)->pluck('name', 'code');
        } else {
            return self::pluck('name', 'code');
        }
    }

    public const MEMBER_FREE = "FREE";
    public const MEMBER_PRO = "PRO";
}
