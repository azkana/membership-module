<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class Supplier extends Model
{
    use TraitsUuid;

    protected $table = 'mbr_suppliers';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'code',
        'name',
        'logo',
        'is_active',
        'created_by',
        'updated_by'
    ];

    public function isActive()
    {
        return $this->is_active = true;
    }
}
