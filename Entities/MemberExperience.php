<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class MemberExperience extends Model
{
    use TraitsUuid;

    protected $table = 'mbr_experiences';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'company',
        'position',
        'work_year',
        'work_month',
        'summary',
        'year_start',
        'month_start',
        'year_end',
        'month_end',
        'is_present',
        'salary',
        'kab_id',
        'prov_id',
        'member_id',
        'created_by',
        'updated_by'
    ];

    public function getMember()
    {
        return $this->belongsTo('Modules\Membership\Entities\Members', 'member_id');
    }

    public function getProvinsi()
    {
        return $this->belongsTo('App\Models\Master\Provinsi', 'prov_id', 'code');
    }

    public static function getWorkYear()
    {
        return [
            1 => '1 Tahun',
            2 => '2 Tahun',
            3 => '3 Tahun',
            4 => '4 Tahun',
            5 => '5 Tahun',
            6 => '6 Tahun',
            7 => '7 Tahun',
            8 => '8 Tahun',
            9 => '9 Tahun',
            10 => '10 Tahun'
        ];
    }

    public static function getWorkMonth()
    {
        return [
            0 => '0 Bulan',
            1 => '1 Bulan',
            2 => '2 Bulan',
            3 => '3 Bulan',
            4 => '4 Bulan',
            5 => '5 Bulan',
            6 => '6 Bulan',
            7 => '7 Bulan',
            8 => '8 Bulan',
            9 => '9 Bulan',
            10 => '10 Bulan',
            11 => '11 Bulan',
            12 => '12 Bulan'
        ];
    }
}
