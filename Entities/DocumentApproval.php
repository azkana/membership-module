<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class DocumentApproval extends Model
{
    use TraitsUuid;

    protected $table = 'mbr_document_approval';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $casts = [
        'doc_status' => 'string',
        'processed_at' => 'datetime'
    ];

    protected $fillable = [
        'doc_status', 'note', 'processed_by', 'processed_at', 'member_id'
    ];

    public function getMember()
    {
        return $this->belongsTo('Modules\Membership\Entities\Members', 'member_id');
    }
}
