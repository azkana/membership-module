<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class Pemanggilan extends Model
{
    use TraitsUuid;

    protected $table = 'mbr_pemanggilan';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'notif_sent',
        'sent_at',
        'expired_at',
        'read_at',
        'confirmed_at',
        'rejected_at',
        'member_id',
        'status',
        'note',
        'created_by',
        'updated_by'
    ];

    public const STATUS_SENT = 'SENT';
    public const STATUS_READ = 'READ';
    public const STATUS_PENDING = 'PENDING';
    public const STATUS_CONFIRMED = 'CONFIRMED';
    public const STATUS_REJECTED = 'REJECTED';
    public const STATUS_EXPIRED = 'EXPIRED';
}
