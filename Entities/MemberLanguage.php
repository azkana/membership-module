<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;

class MemberLanguage extends Model
{
    protected $table = 'mbr_languages';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'name',
        'spoken',
        'written',
        'member_id',
        'created_by',
        'updated_by'
    ];

    public function getMember()
    {
        return $this->belongsTo('Modules\Membership\Entities\Members', 'member_id');
    }
}
