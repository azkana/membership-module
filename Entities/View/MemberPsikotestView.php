<?php

namespace Modules\Membership\Entities\View;

use Illuminate\Database\Eloquent\Model;

class MemberPsikotestView extends Model
{
    protected $table = 'vw_mbr_psi';

    protected $guard = 'member';

    public $incrementing    = false;

}
