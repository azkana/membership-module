<?php

namespace Modules\Membership\Entities\View;

use Illuminate\Database\Eloquent\Model;

class MemberAvailableView extends Model
{
    protected $table = 'vw_mbr_ava';

    protected $guard = 'member';

    public $incrementing    = false;

}
