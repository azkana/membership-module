<?php

namespace Modules\Membership\Entities\View;

use Illuminate\Database\Eloquent\Model;

class MemberPemanggilanView extends Model
{
    protected $table = 'vw_mbr_pem';

    protected $guard = 'member';

    public $incrementing    = false;

}
