<?php

namespace Modules\Membership\Entities\View;

use Illuminate\Database\Eloquent\Model;

class MemberMcuView extends Model
{
    protected $table = 'vw_mbr_mcu';

    protected $guard = 'member';

    public $incrementing    = false;

}
