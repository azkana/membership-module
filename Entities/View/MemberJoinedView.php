<?php

namespace Modules\Membership\Entities\View;

use Illuminate\Database\Eloquent\Model;

class MemberJoinedView extends Model
{
    protected $table = 'vw_mbr_end';

    protected $guard = 'member';

    public $incrementing    = false;

}
