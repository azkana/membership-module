<?php

namespace Modules\Membership\Entities\View;

use Illuminate\Database\Eloquent\Model;

class MemberPemanggilanKonfirmasiView extends Model
{
    protected $table = 'vw_mbr_kon';

    protected $guard = 'member';

    public $incrementing    = false;

}
