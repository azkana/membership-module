<?php

namespace Modules\Membership\Entities\View;

use Illuminate\Database\Eloquent\Model;

class MemberInterviewView extends Model
{
    protected $table = 'vw_mbr_int';

    protected $guard = 'member';

    public $incrementing    = false;

}
