<?php

namespace Modules\Membership\Entities;

use App\Models\Configs\Sequence;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;

class MemberOrder extends Model
{
    use TraitsUuid;

    protected $table = 'mbr_orders';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'code',
        'amount',
        'amount_uniq',
        'discount_percent',
        'discount_amount',
        'grand_total',
        'order_date',
        'status',
        'due_date',
        'payment_status',
        'payment_token',
        'payment_url',
        'note',
        'member_id',
        'approved_by',
        'approved_at'
    ];

    public function getMember()
    {
        return $this->belongsTo('Modules\Membership\Entities\Members', 'member_id', 'id');
    }

    public function hasPayment()
    {
        return $this->hasMany('Modules\Membership\Entities\MemberOrder', 'order_id', 'order_id');
    }

    public const CREATED = 'created';
	public const CONFIRMED = 'confirmed';
	public const DELIVERED = 'delivered';
	public const COMPLETED = 'completed';
	public const CANCELLED = 'cancelled';

	public const ORDERCODE = 'INV';

	public const PAID = 'paid';
	public const UNPAID = 'unpaid';

    public static function _generateOrderNumber()
    {
        $getSequenceNumber = Sequence::_getNextVal('membership', 'member_order', date('m'));
        $prefix = self::ORDERCODE;
        $del    = '/';
        $month  = date('m');
        $year   = date('Y');
        $increment = str_pad($getSequenceNumber, 4, '0', STR_PAD_LEFT);

        $orderNumber = $year . $month . $increment . $del . $prefix . $del . monthToRoman($month) . $del . $year;
        return $orderNumber;
    }

    public function isPaid()
	{
		return $this->status == self::PAID;
	}
}
