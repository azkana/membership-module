<?php

namespace Modules\Membership\Entities;

use App\Models\Configs\Sequence;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MailResetPasswordNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use App\Traits\Uuid as TraitsUuid;

class Members extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use TraitsUuid;

    protected $table = 'mbr_members';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $fillable = [
        'no_member', 'first_name', 'last_name', 'email', 'password', 'verified_at', 
        'membership', 'doc_status', 'rec_status', 'is_active'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $dates = [
        'verified_at'
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token));
    }

    public function passwordSecurity()
    {
        return $this->hasOne('App\Models\PasswordSecurity');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function hasData()
    {
        return $this->hasOne('Modules\Membership\Entities\MemberData', 'member_id');
    }

    public function hasAddress()
    {
        return $this->hasMany('Modules\Membership\Entities\MemberAddress', 'member_id');
    }

    public function hasExperience()
    {
        return $this->hasMany('Modules\Membership\Entities\MemberExperience', 'member_id');
    }

    public function hasEducation()
    {
        return $this->hasMany('Modules\Membership\Entities\MemberEducation', 'member_id');
    }

    public function hasSkill()
    {
        return $this->hasMany('Modules\Membership\Entities\MemberSkill', 'member_id');
    }

    public function hasLanguage()
    {
        return $this->hasMany('Modules\Membership\Entities\MemberLanguage', 'member_id');
    }

    public function hasAddInfo()
    {
        return $this->hasOne('Modules\Membership\Entities\MemberAddInfo', 'member_id');
    }

    public function hasDocApproval()
    {
        return $this->hasMany('Modules\Membership\Entities\DocumentApproval', 'member_id');
    }
    public function hasPemanggilan()
    {
        return $this->hasMany('Modules\Membership\Entities\Pemanggilan', 'member_id');
    }

    public const REC_STATUS_AVAILABLE   = 'AVA';
    public const REC_STATUS_PEMANGGILAN = 'PEM';
    public const REC_STATUS_KONFIRMASI  = 'KON';
    public const REC_STATUS_PSIKOTEST   = 'PSI';
    public const REC_STATUS_INTERVIEW   = 'INT';
    public const REC_STATUS_MCU         = 'MCU';
    public const REC_STATUS_JOINED      = 'JOI';

    public const DOC_STATUS_NONE        = 'NONE';
    public const DOC_STATUS_PENDING     = 'PENDING';
    public const DOC_STATUS_APPROVED    = 'APPROVED';
    public const DOC_STATUS_REJECTED    = 'REJECTED';

    public static function _generateMemberNumber()
    {
        $getSequenceNumber = Sequence::_getNextVal('membership', 'member_number', date('y'));
        $period = date('y');
        $increment = str_pad($getSequenceNumber, 5, '0', STR_PAD_LEFT);
        $memberNumber = $period . $increment;

        return $memberNumber;
    }

}
