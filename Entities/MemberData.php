<?php

namespace Modules\Membership\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid as TraitsUuid;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MemberData extends Model
{
    use TraitsUuid;

    protected $table = 'mbr_data';

    protected $guard = 'member';

    public $incrementing    = false;

    protected $dates = [
        'b_date'
    ];
    
    protected $fillable = [
        'nik',
        'phone',
        'wa',
        'b_place',
        'b_date',
        'gender',
        'tb',
        'bb',
        'npwp',
        'kk',
        'religion_id',
        'marital_id',
        'attc_card',
        'attc_foto',
        'attc_npwp',
        'attc_kk',
        'attc_cv',
        'nationality',
        'member_id',
        'created_by',
        'updated_by'
    ];

    public function getMember()
    {
        return $this->belongsTo('Modules\Membership\Entities\Members', 'member_id');
    }

    public function getAgama()
    {
        return $this->belongsTo('App\Models\Master\Agama', 'religion_id');
    }

    public function getMarital()
    {
        return $this->belongsTo('App\Models\Master\Marital', 'marital_id');
    }
}
