@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')

    <tr>
        <td class="title">
            Hi, <strong>{!! $data['full_name'] !!}</strong>
        </td>
    </tr>

    <tr>
        <td>
            Selamat {!! $data['full_name'] !!}, kamu sudah berhasil bergabung dengan jobsdku. Silahkan verifikasi email kamu dengan cara klik tombol dibawah.
            Terima kasih.
        </td>
    </tr>

    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td>
            @include('beautymail::templates.minty.button', ['text' => 'Verifikasi Sekarang', 'link' => route('membership.email.verify', $data['member_id'])])
            <img src="{!! route('email.read', 'id='.$data['log_id']) !!}" width="1" height="1" />
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    @include('beautymail::templates.minty.contentEnd')

@stop