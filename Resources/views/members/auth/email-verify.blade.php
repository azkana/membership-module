@extends('membership::members.layouts.app-login')

@section('content')
    
    <div class="d-flex flex-column-fluid flex-center">
        <div class="bg-success-o-50 p-5">
            <span>Hi {!! $data->full_name !!}, Terima kasih sudah mendaftar di jobsdku. {!! $message !!}</span>
        </div>
    </div>

@endsection