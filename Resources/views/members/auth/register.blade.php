@extends('membership::members.layouts.app-login')

@section('content')
<div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">
    {{-- begin::Head --}}
    <div class="kt-login__head">
        <span class="kt-login__signup-label">Sudah punya akun?</span>&nbsp;&nbsp;
        <a href="{!! route('membership.login') !!}" class="kt-link kt-login__signup-link">Login</a>
    </div>
    {{-- end::Head --}}

    {{-- begin::Body --}}
    <div class="kt-login__body">
        {{-- begin::Signin --}}
        <div class="kt-login__form">
            <div class="kt-login__title">
                <h3>Register</h3>
            </div>
            {{-- begin::Form --}}
            <form action="#" method="POST" class="kt-form" id="kt-login__form" novalidate="novalidate">
                @csrf
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="Username" name="email" id="email" autocomplete="on">
                </div>
                <div class="form-group">
                    <input class="form-control" type="password" placeholder="Password" name="password" id="password">
                </div>
                <div class="kt-login__actions">
                    <a href="#" class="kt-link kt-login__link-forgot">
                        Forgot Password ?
                    </a>
                    <button id="kt_login_signin_submit" class="btn btn-primary btn-elevate kt-login__btn-primary">Log In</button>
                </div>
            </form>
            {{-- end::Form --}}
            {{-- <div class="kt-login__divider">
                <div class="kt-divider">
                    <span></span>
                    <span>OR</span>
                    <span></span>
                </div>
            </div> --}}

            {{-- <div class="kt-login__options">
                <a href="#" class="btn btn-primary kt-btn">
                    <i class="fab fa-facebook-f"></i>
                    Facebook
                </a>
                <a href="#" class="btn btn-info kt-btn">
                    <i class="fab fa-twitter"></i>
                    Twitter
                </a>
                <a href="#" class="btn btn-danger kt-btn">
                    <i class="fab fa-google"></i>
                    Google
                </a>
            </div> --}}
        </div>
        {{-- end::Signin --}}
    </div>
    {{-- end::Body --}}
</div>
@endsection