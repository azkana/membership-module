@extends('membership::members.layouts.app')

@section('content')

    @include('membership::members.biodata.wizard-warning')
    @include('membership::members.biodata.wizard-step')

    <div class="d-flex flex-column-fluid">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">Alamat</h3>
                        </div>
                        <form action="{!! route('membership.biodata.address.save') !!}" method="POST" id="form_wizard_2" autocomplete="off">
                            @csrf
                            <div class="card-body">
                                
                                @include('membership::members.biodata.wizard-alert')
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">Alamat Sesuai KTP:</h3>
                                        <div class="mb-15">
                                            <div class="form-group row">
                                                <label for="line1" class="col-3 col-form-label">Alamat <span class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    {!! Form::text('line1', null, ['class' => 'form-control text-uppercase', 'id' => 'line1', 'required']) !!}
                                                </div>
                                            </div>
                                            {{-- <div class="form-group row">
                                                <label for="line2" class="col-3 col-form-label"></label>
                                                <div class="col-8">
                                                    {!! Form::text('line2', null, ['class' => 'form-control', 'id' => 'line2']) !!}
                                                </div>
                                            </div> --}}
                                            <div class="form-group row">
                                                <label for="blok" class="col-3 col-form-label">Blok</label>
                                                <div class="col-3">
                                                    {!! Form::text('blok', null, ['class' => 'form-control text-uppercase', 'id' => 'blok']) !!}
                                                </div>
                                                <label for="nomor" class="col-2 col-form-label">Nomor</label>
                                                <div class="col-3">
                                                    {!! Form::text('nomor', null, ['class' => 'form-control text-uppercase', 'id' => 'nomor']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="rt" class="col-3 col-form-label">RT <span class="text-danger">*</span></label>
                                                <div class="col-3">
                                                    {!! Form::text('rt', null, ['class' => 'form-control', 'id' => 'rt', 'required']) !!}
                                                </div>
                                                <label for="rw" class="col-2 col-form-label">RW <span class="text-danger">*</span></label>
                                                <div class="col-3">
                                                    {!! Form::text('rw', null, ['class' => 'form-control', 'id' => 'rw', 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="provinsi" class="col-3 col-form-label">Provinsi <span class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    {!! Form::select('provinsi', $provinsi, null, [ 'class' => 'form-control text-uppercase selectpicker', 'id' => 'provinsi', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="kabupaten" class="col-3 col-form-label">Kabupaten <span class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    {!! Form::select('kabupaten', [], null, [ 'class' => 'form-control text-uppercase selectpicker', 'id' => 'kabupaten', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="kecamatan" class="col-3 col-form-label">Kecamatan <span class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    {!! Form::select('kecamatan', [], null, [ 'class' => 'form-control text-uppercase selectpicker', 'id' => 'kecamatan', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="kelurahan" class="col-3 col-form-label">Kelurahan <span class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    {!! Form::select('kelurahan', [], null, [ 'class' => 'form-control text-uppercase selectpicker', 'id' => 'kelurahan', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="kodepos" class="col-3 col-form-label">Kodepos</label>
                                                <div class="col-4">
                                                    {!! Form::text('kodepos', null, ['class' => 'form-control', 'id' => 'kodepos', 'required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h3 class="font-size-lg text-dark font-weight-bold mb-6">Alamat Domisili:</h3>
                                        <div class="mb-15">
                                            {{-- <div class="form-group row">
                                                <div class="col-10 col-form-label">
                                                    <div class="checkbox-inline">
                                                        <label class="checkbox">
															<input type="checkbox" name="equalToKtp" id="equalToKtp" />
															<span></span>sama dengan alamat KTP
                                                        </label>
                                                    </div>
                                                </div>
                                            </div> --}}
                                            <div class="form-group row">
                                                <label for="dom_line1" class="col-3 col-form-label">Alamat <span class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    {!! Form::text('dom_line1', null, ['class' => 'form-control text-uppercase', 'id' => 'dom_line1', 'required']) !!}
                                                </div>
                                            </div>
                                            {{-- <div class="form-group row">
                                                <label for="dom_line2" class="col-3 col-form-label"></label>
                                                <div class="col-8">
                                                    {!! Form::text('dom_line2', null, ['class' => 'form-control', 'id' => 'dom_line2']) !!}
                                                </div>
                                            </div> --}}
                                            <div class="form-group row">
                                                <label for="dom_blok" class="col-3 col-form-label">Blok</label>
                                                <div class="col-3">
                                                    {!! Form::text('dom_blok', null, ['class' => 'form-control text-uppercase', 'id' => 'dom_blok', 'required']) !!}
                                                </div>
                                                <label for="dom_nomor" class="col-2 col-form-label">Nomor</label>
                                                <div class="col-3">
                                                    {!! Form::text('dom_nomor', null, ['class' => 'form-control text-uppercase', 'id' => 'dom_nomor', 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="dom_rt" class="col-3 col-form-label">RT <span class="text-danger">*</span></label>
                                                <div class="col-3">
                                                    {!! Form::text('dom_rt', null, ['class' => 'form-control', 'id' => 'dom_rt', 'required']) !!}
                                                </div>
                                                <label for="dom_rw" class="col-2 col-form-label">RW <span class="text-danger">*</span></label>
                                                <div class="col-3">
                                                    {!! Form::text('dom_rw', null, ['class' => 'form-control', 'id' => 'dom_rw', 'required']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="dom_provinsi" class="col-3 col-form-label">Provinsi <span class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    {!! Form::select('dom_provinsi', $provinsi, null, [ 'class' => 'form-control text-uppercase selectpicker', 'id' => 'dom_provinsi', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="dom_kabupaten" class="col-3 col-form-label">Kabupaten <span class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    {!! Form::select('dom_kabupaten', [], null, [ 'class' => 'form-control text-uppercase selectpicker', 'id' => 'dom_kabupaten', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="dom_kecamatan" class="col-3 col-form-label">Kecamatan <span class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    {!! Form::select('dom_kecamatan', [], null, [ 'class' => 'form-control text-uppercase selectpicker', 'id' => 'dom_kecamatan', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="dom_kelurahan" class="col-3 col-form-label">Kelurahan <span class="text-danger">*</span></label>
                                                <div class="col-8">
                                                    {!! Form::select('dom_kelurahan', [], null, [ 'class' => 'form-control text-uppercase selectpicker', 'id' => 'dom_kelurahan', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="dom_kodepos" class="col-3 col-form-label">Kodepos</label>
                                                <div class="col-4">
                                                    {!! Form::text('dom_kodepos', null, ['class' => 'form-control', 'id' => 'dom_kodepos', 'required']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success mr-2">Simpan & Lanjut</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    <link href="{!! asset('modules/membership/theme/css/pages/wizard/wizard-1.css') !!}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{!! asset('modules/membership/theme/js/pages/crud/forms/validation/biodata-wizard-2.js') !!}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            // var equalToKtp = $("#equalToKtp");
            var provinsi = $("#provinsi");
            var kabupaten = $("#kabupaten");
            var kecamatan = $("#kecamatan");
            var kelurahan = $("#kelurahan");
            var dom_provinsi = $("#dom_provinsi");
            var dom_kabupaten = $("#dom_kabupaten");
            var dom_kecamatan = $("#dom_kecamatan");
            var dom_kelurahan = $("#dom_kelurahan");
            let placeHolder = '<option value="">Pilih</option>';

            // equalToKtp.on('click', function(e) {
            //     if(this.checked == false) {
            //         $("#group-dom").removeClass('d-none');
            //     } else {
            //         copyAddress();
            //     }
            // });

            provinsi.on('change', function(e) {
                e.preventDefault();
                let provId = e.target.value;
                if(provId !== '') {
                    $.ajax({
                        url: "{!! route('kabupaten.list') !!}",
                        type: "GET",
                        data: { prov_id: provId },
                        success: function (data) {
                            kabupaten.empty().append(placeHolder);
                            $.each(data, function(index, kab) {
                                kabupaten.append('<option value="' + kab.code + '">' + kab.name + '</option>').selectpicker('refresh');
                            });
                            kecamatan.empty().append(placeHolder).selectpicker('refresh');
                            kelurahan.empty().append(placeHolder).selectpicker('refresh');
                        }
                    });
                } else {
                    kabupaten.empty().append(placeHolder).selectpicker('refresh');
                    kecamatan.empty().append(placeHolder).selectpicker('refresh');
                    kelurahan.empty().append(placeHolder).selectpicker('refresh');
                }
            });
            kabupaten.on('change', function(e) {
                e.preventDefault();
                let kabId = e.target.value;
                if(kabId !== '') {
                    $.ajax({
                        url: "{!! route('kecamatan.list') !!}",
                        type: "GET",
                        data: { kab_id: kabId },
                        success: function (data) {
                            kecamatan.empty().append(placeHolder);
                            $.each(data, function(index, kec) {
                                kecamatan.append('<option value="' + kec.code + '">' + kec.name + '</option>').selectpicker('refresh');
                            });
                            kelurahan.empty().append(placeHolder).selectpicker('refresh');
                        }
                    });
                } else {
                    kecamatan.empty().append(placeHolder).selectpicker('refresh');
                    kelurahan.empty().append(placeHolder).selectpicker('refresh');
                }
            });
            kecamatan.on('change', function(e) {
                e.preventDefault();
                let id = e.target.value;
                if(id !== '') {
                    $.ajax({
                        url: "{!! route('kelurahan.list') !!}",
                        type: "GET",
                        data: { kec_id: id },
                        success: function (data) {
                            kelurahan.empty().append(placeHolder);
                            $.each(data, function(index, kel) {
                                kelurahan.append('<option value="' + kel.code + '">' + kel.name + '</option>').selectpicker('refresh');
                            });
                        }
                    });
                } else {
                    kelurahan.empty().append(placeHolder).selectpicker('refresh');
                }
            });
            // Alamat Domisili
            dom_provinsi.on('change', function(e) {
                e.preventDefault();
                let provId = e.target.value;
                if(provId !== '') {
                    $.ajax({
                        url: "{!! route('kabupaten.list') !!}",
                        type: "GET",
                        data: { prov_id: provId },
                        success: function (data) {
                            dom_kabupaten.empty().append(placeHolder);
                            $.each(data, function(index, kab) {
                                dom_kabupaten.append('<option value="' + kab.code + '">' + kab.name + '</option>').selectpicker('refresh');
                            });
                            dom_kecamatan.empty().append(placeHolder).selectpicker('refresh');
                            dom_kelurahan.empty().append(placeHolder).selectpicker('refresh');
                        }
                    });
                } else {
                    dom_kabupaten.empty().append(placeHolder).selectpicker('refresh');
                    dom_kecamatan.empty().append(placeHolder).selectpicker('refresh');
                    dom_kelurahan.empty().append(placeHolder).selectpicker('refresh');
                }
            });
            dom_kabupaten.on('change', function(e) {
                e.preventDefault();
                let kabId = e.target.value;
                if(kabId !== '') {
                    $.ajax({
                        url: "{!! route('kecamatan.list') !!}",
                        type: "GET",
                        data: { kab_id: kabId },
                        success: function (data) {
                            dom_kecamatan.empty().append(placeHolder);
                            $.each(data, function(index, kec) {
                                dom_kecamatan.append('<option value="' + kec.code + '">' + kec.name + '</option>').selectpicker('refresh');
                            });
                            dom_kelurahan.empty().append(placeHolder).selectpicker('refresh');
                        }
                    });
                } else {
                    dom_kecamatan.empty().append(placeHolder).selectpicker('refresh');
                    dom_kelurahan.empty().append(placeHolder).selectpicker('refresh');
                }
            });
            dom_kecamatan.on('change', function(e) {
                e.preventDefault();
                let id = e.target.value;
                if(id !== '') {
                    $.ajax({
                        url: "{!! route('kelurahan.list') !!}",
                        type: "GET",
                        data: { kec_id: id },
                        success: function (data) {
                            dom_kelurahan.empty().append(placeHolder);
                            $.each(data, function(index, kel) {
                                dom_kelurahan.append('<option value="' + kel.code + '">' + kel.name + '</option>').selectpicker('refresh');
                            });
                        }
                    });
                } else {
                    dom_kelurahan.empty().append(placeHolder).selectpicker('refresh');
                }
            });

            // function copyAddress() {
            //     var line1 = $('#line1');
            //     var line2 = $('#line2');
            //     var rt = $('#rt');
            //     var rw = $('#rw');
            //     var blok = $('#blok');
            //     var nomor = $('#nomor');
            //     var provinsi = $('#provinsi');
            //     var kabupaten = $('#kabupaten');
            //     var kecamatan = $('#kecamatan');

            //     $('#dom_line1').val(line1.val());
            //     $('#dom_line2').val(line2.val());
            //     $('#dom_rt').val(rt.val());
            //     $('#dom_rw').val(rw.val());
            //     $('#dom_blok').val(blok.val());
            //     $('#dom_nomor').val(nomor.val());
            //     $('#dom_provinsi').selectpicker('val', provinsi.val());
            //     dom_provinsi.change().selectpicker('refresh');
            //     $('#dom_kabupaten').selectpicker('val', kabupaten.val());
            //     dom_kabupaten.change().selectpicker('refresh');
            //     $('#dom_kecamatan').selectpicker('val', kecamatan.val());
            //     dom_kecamatan.change().selectpicker('refresh');
            // }
        });
    </script>
@endsection