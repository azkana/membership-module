@extends('membership::members.layouts.app')

@section('content')

    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="alert alert-warning" role="alert">
                Hai <b>{!! Auth::user()->full_name !!}</b>, 
                Biodata kamu belum lengkap, silahkan lengkapi biodata kamu, agar kami bisa tahu jika ada lowongan yang cocok untuk kamu.
            </div>
            <div class="card card-custom">
                <div class="card-body p-0">
                    <div class="wizard wizard-2" id="kt_wizard" data-wizard-state="step-first" data-wizard-clickable="false">
                        <div class="wizard-nav border-right py-8 px-8 py-lg-20 px-lg-10">
                            <div class="wizard-steps">
                                <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                    <div class="wizard-wrapper">
                                        <div class="wizard-icon">
                                            <span class="svg-icon svg-icon-2x">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <polygon points="0 0 24 0 24 24 0 24" />
                                                        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">Biodata</h3>
                                            <div class="wizard-desc">Lengkapi Biodata</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wizard-step" data-wizard-type="step">
                                    <div class="wizard-wrapper">
                                        <div class="wizard-icon">
                                            <span class="svg-icon svg-icon-2x">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">Alamat</h3>
                                            <div class="wizard-desc">Lengkapi Alamat Sesuai KTP</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wizard-step" data-wizard-type="step">
                                    <div class="wizard-wrapper">
                                        <div class="wizard-icon">
                                            <span class="svg-icon svg-icon-2x">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M5,10.5 C5,6 8,3 12.5,3 C17,3 20,6.75 20,10.5 C20,12.8325623 17.8236613,16.03566 13.470984,20.1092932 C12.9154018,20.6292577 12.0585054,20.6508331 11.4774555,20.1594925 C7.15915182,16.5078313 5,13.2880005 5,10.5 Z M12.5,12 C13.8807119,12 15,10.8807119 15,9.5 C15,8.11928813 13.8807119,7 12.5,7 C11.1192881,7 10,8.11928813 10,9.5 C10,10.8807119 11.1192881,12 12.5,12 Z" fill="#000000" fill-rule="nonzero"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">Alamat Domisili</h3>
                                            <div class="wizard-desc">Lengkapi Alamat Domisili (Tempat Tinggal)</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wizard-step" data-wizard-type="step">
                                    <div class="wizard-wrapper">
                                        <div class="wizard-icon">
                                            <span class="svg-icon svg-icon-2x">
                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="0" y="0" width="24" height="24"/>
                                                        <path d="M13.0799676,14.7839934 L15.2839934,12.5799676 C15.8927139,11.9712471 16.0436229,11.0413042 15.6586342,10.2713269 L15.5337539,10.0215663 C15.1487653,9.25158901 15.2996742,8.3216461 15.9083948,7.71292558 L18.6411989,4.98012149 C18.836461,4.78485934 19.1530435,4.78485934 19.3483056,4.98012149 C19.3863063,5.01812215 19.4179321,5.06200062 19.4419658,5.11006808 L20.5459415,7.31801948 C21.3904962,9.0071287 21.0594452,11.0471565 19.7240871,12.3825146 L13.7252616,18.3813401 C12.2717221,19.8348796 10.1217008,20.3424308 8.17157288,19.6923882 L5.75709327,18.8875616 C5.49512161,18.8002377 5.35354162,18.5170777 5.4408655,18.2551061 C5.46541191,18.1814669 5.50676633,18.114554 5.56165376,18.0596666 L8.21292558,15.4083948 C8.8216461,14.7996742 9.75158901,14.6487653 10.5215663,15.0337539 L10.7713269,15.1586342 C11.5413042,15.5436229 12.4712471,15.3927139 13.0799676,14.7839934 Z" fill="#000000"/>
                                                        <path d="M14.1480759,6.00715131 L13.9566988,7.99797396 C12.4781389,7.8558405 11.0097207,8.36895892 9.93933983,9.43933983 C8.8724631,10.5062166 8.35911588,11.9685602 8.49664195,13.4426352 L6.50528978,13.6284215 C6.31304559,11.5678496 7.03283934,9.51741319 8.52512627,8.02512627 C10.0223249,6.52792766 12.0812426,5.80846733 14.1480759,6.00715131 Z M14.4980938,2.02230302 L14.313049,4.01372424 C11.6618299,3.76737046 9.03000738,4.69181803 7.1109127,6.6109127 C5.19447112,8.52735429 4.26985715,11.1545872 4.51274152,13.802405 L2.52110319,13.985098 C2.22450978,10.7517681 3.35562581,7.53777247 5.69669914,5.19669914 C8.04101739,2.85238089 11.2606138,1.72147333 14.4980938,2.02230302 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                                                    </g>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">Kontak Darurat</h3>
                                            <div class="wizard-desc">Lengkapi Kontak Darurat</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wizard-step" data-wizard-type="step">
                                    <div class="wizard-wrapper">
                                        <div class="wizard-icon">
                                            <i class="la la-university icon-xl"></i>
                                        </div>
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">Pendidikan</h3>
                                            <div class="wizard-desc">Lengkapi Pendidikan</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                            <div class="row">
                                <div class="offset-xxl-2 col-xxl-8">
                                    <form class="form" id="kt_form" autocomplete="off">
                                        <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>NIK</label>
                                                        <input type="text" name="nik" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasData) ? $data->hasData->nik : null !!}" maxlength="16" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>No. KK</label>
                                                        <input type="text" name="kk" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasData) ? $data->hasData->kk : null !!}" maxlength="16" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Nama Depan</label>
                                                        <input type="text" name="fname" class="form-control form-control-solid form-control-md" value="{!! $data->first_name !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Nama Belakang</label>
                                                        <input type="text" name="lname" class="form-control form-control-solid form-control-md" value="{!! $data->last_name !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Tempat Lahir</label>
                                                        <input type="text" name="b_place" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasData) ? $data->hasData->b_place : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Tgl. Lahir</label>
                                                        <input type="date" name="b_date" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasData) ? $data->hasData->b_date : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>No. HP</label>
                                                        <input type="text" name="no_hp" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasData) ? $data->hasData->phone : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>No. WA</label>
                                                        <input type="text" name="no_wa" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasData) ? $data->hasData->wa : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Jenis Kelamin</label>
                                                <div class="radio-inline">
                                                    <label class="radio">
                                                        <input type="radio" name="gender" /> 
                                                        <span></span> Laki-Laki
                                                    </label>
                                                    <label class="radio">
                                                        <input type="radio" name="gender" />
                                                        <span></span> Perempuan
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Berat Badan</label>
                                                        <input type="text" name="bb" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasData) ? $data->hasData->bb : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Tinggi Badan</label>
                                                        <input type="text" name="tb" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasData) ? $data->hasData->tb : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>NPWP</label>
                                                        <input type="text" name="npwp" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasData) ? $data->hasData->npwp : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kewarganegaraan</label>
                                                        {!! Form::select('wn', $nationality, isset($data->hasData) ? $data->hasData->nationality : null, [ 'class' => 'form-control form-control-solid form-control-md', 'id' => 'wn', 'placeholder' => 'Pilih']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Agama</label>
                                                        {!! Form::select('religion', $religion, isset($data->hasData) ? $data->hasData->religion_id : null, [ 'class' => 'form-control form-control-solid form-control-md', 'id' => 'religion', 'placeholder' => 'Pilih']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Status Perkawinan</label>
                                                        {!! Form::select('marital', $marital, isset($data->hasData) ? $data->hasData->marital_id : null, [ 'class' => 'form-control form-control-solid form-control-md', 'id' => 'marital', 'placeholder' => 'Pilih']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <div class="form-group">
                                                <label>Alamat </label>
                                                <input type="text" name="addr_ktp_line1" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasAddress->line1) ? $data->hasAddress->line1 : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label> </label>
                                                <input type="text" name="addr_ktp_line2" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasAddress->line2) ? $data->hasAddress->line2 : null !!}" />
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>RT</label>
                                                        <input type="text" name="addr_ktp_rt" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasAddress->rt) ? $data->hasAddress->rt : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>RW</label>
                                                        <input type="text" name="addr_ktp_rw" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasAddress->rw) ? $data->hasAddress->rw : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Blok</label>
                                                        <input type="text" name="addr_ktp_blok" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasAddress->blok) ? $data->hasAddress->blok : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Nomor</label>
                                                        <input type="text" name="addr_ktp_no" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasAddress->no) ? $data->hasAddress->no : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Provinsi</label>
                                                        {!! Form::select('addr_ktp_prov', $provinsi, isset($data->hasAddress->prov_id) ? $data->hasAddress->prov_id : null, [ 'class' => 'form-control form-control-solid form-control-md', 'id' => 'addr_ktp_prov', 'placeholder' => 'Pilih']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kabupaten / Kota</label>
                                                        <input type="text" name="addr_ktp_kab" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasAddress->kab_id) ? $data->hasAddress->kab_id : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kecamatan</label>
                                                        <input type="text" name="addr_ktp_kec" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasAddress->kec_id) ? $data->hasAddress->kec_id : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kelurahan</label>
                                                        <input type="text" name="addr_ktp_kel" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasAddress->kel_id) ? $data->hasAddress->kel_id : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kodepos</label>
                                                        <input type="text" name="addr_ktp_zip" class="form-control form-control-solid form-control-md" value="{!! isset($data->hasAddress->zipcode) ? $data->hasAddress->zipcode : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <div class="form-group">
                                                <label>Alamat </label>
                                                <input type="text" name="addr_dom_line1" class="form-control form-control-solid form-control-md" value="" />
                                            </div>
                                            <div class="form-group">
                                                <label> </label>
                                                <input type="text" name="addr_dom_line2" class="form-control form-control-solid form-control-md" value="" />
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>RT</label>
                                                        <input type="text" name="addr_dom_rt" class="form-control form-control-solid form-control-md" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>RW</label>
                                                        <input type="text" name="addr_dom_rw" class="form-control form-control-solid form-control-md" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Blok</label>
                                                        <input type="text" name="addr_dom_blok" class="form-control form-control-solid form-control-md" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Nomor</label>
                                                        <input type="text" name="addr_dom_no" class="form-control form-control-solid form-control-md" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Provinsi</label>
                                                        <input type="text" name="addr_dom_prov" class="form-control form-control-solid form-control-md" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kabupaten / Kota</label>
                                                        <input type="text" name="addr_dom_kab" class="form-control form-control-solid form-control-md" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kecamatan</label>
                                                        <input type="text" name="addr_dom_kec" class="form-control form-control-solid form-control-md" value="" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kelurahan</label>
                                                        <input type="text" name="addr_dom_kel" class="form-control form-control-solid form-control-md" value="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text" name="emr_name" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->phone : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label>No. Telpon</label>
                                                <input type="text" name="emr_phone" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->phone : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label>Alamat </label>
                                                <input type="text" name="emr_line1" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->phone : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label> </label>
                                                <input type="text" name="emr_line2" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->phone : null !!}" />
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>RT</label>
                                                        <input type="text" name="emr_rt" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->nik : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>RW</label>
                                                        <input type="text" name="emr_rw" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->kk : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Blok</label>
                                                        <input type="text" name="emr_blok" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->nik : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Nomor</label>
                                                        <input type="text" name="emr_no" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->kk : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Provinsi</label>
                                                        <input type="text" name="emr_prov" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->nik : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kabupaten / Kota</label>
                                                        <input type="text" name="emr_kab" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->kk : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kecamatan</label>
                                                        <input type="text" name="emr_kec" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->nik : null !!}" />
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Kelurahan</label>
                                                        <input type="text" name="emr_kel" class="form-control form-control-solid form-control-md" value="{!! !is_null($data->hasData) ? $data->hasData->kk : null !!}" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pb-5" data-wizard-type="step-content">
                                            <div class="form-group">
                                                <label>Jenjang </label>
                                                <input type="text" name="edu_degree" class="form-control form-control-solid form-control-md" value="{!! !isset($data->hasEducation) ? $data->hasEducation->degree : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label>Institusi </label>
                                                <input type="text" name="edu_degree" class="form-control form-control-solid form-control-md" value="{!! !isset($data->hasEducation) ? $data->hasEducation->institution : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label>Jurusan </label>
                                                <input type="text" name="edu_degree" class="form-control form-control-solid form-control-md" value="{!! !isset($data->hasEducation) ? $data->hasEducation->majors : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label>Tahun Masuk </label>
                                                <input type="text" name="edu_degree" class="form-control form-control-solid form-control-md" value="{!! !isset($data->hasEducation) ? $data->hasEducation->start_year : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label>Tahun Lulus </label>
                                                <input type="text" name="edu_degree" class="form-control form-control-solid form-control-md" value="{!! !isset($data->hasEducation) ? $data->hasEducation->graduation : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label>IPK </label>
                                                <input type="text" name="edu_degree" class="form-control form-control-solid form-control-md" value="{!! !isset($data->hasEducation) ? $data->hasEducation->grade : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label>Provinsi </label>
                                                <input type="text" name="edu_degree" class="form-control form-control-solid form-control-md" value="{!! !isset($data->hasEducation) ? $data->hasEducation->prov_id : null !!}" />
                                            </div>
                                            <div class="form-group">
                                                <label>Kabupaten </label>
                                                <input type="text" name="edu_degree" class="form-control form-control-solid form-control-md" value="{!! !isset($data->hasEducation) ? $data->hasEducation->kab_id : null !!}" />
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                            <div class="mr-2">
                                                <button type="button" class="btn btn-light-primary btn-sm font-weight-bolder text-uppercase" data-wizard-type="action-prev">Previous</button>
                                            </div>
                                            <div>
                                                <button type="button" class="btn btn-success btn-sm font-weight-bolder text-uppercase" data-wizard-type="action-submit">Submit</button>
                                                <button type="button" class="btn btn-primary btn-sm font-weight-bolder text-uppercase" data-wizard-type="action-next">Next</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    <link href="{!! asset('modules/membership/theme/css/pages/wizard/wizard-2.css') !!}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{!! asset('modules/membership/theme/js/pages/custom/wizard/wizard-2.js') !!}" type="text/javascript"></script>
@endsection