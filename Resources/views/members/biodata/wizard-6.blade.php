@extends('membership::members.layouts.app')

@section('content')

    @include('membership::members.biodata.wizard-warning')
    @include('membership::members.biodata.wizard-step')

    <div class="d-flex flex-column-fluid">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">Dokumen Pendukung</h3>
                        </div>
                        <form action="{!! route('membership.biodata.document.save') !!}" method="POST" id="form_wizard_6" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">

                                @include('membership::members.biodata.wizard-alert')
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="foto" class="col-3 col-form-label">Pas Foto <span class="text-danger">*</span></label>
                                            <div class="col-lg-8 col-md-9 col-sm-12">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="foto" id="foto" accept=".png, .jpg, .jpeg" />
                                                    <label class="custom-file-label" for="foto">Pilih file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="ktp" class="col-3 col-form-label">KTP <span class="text-danger">*</span></label>
                                            <div class="col-lg-8 col-md-9 col-sm-12">
                                                <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="ktp" id="ktp" accept=".png, .jpg, .jpeg" />
                                                    <label class="custom-file-label" for="ktp">Pilih file</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"></div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success mr-2">Simpan & Lanjut</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    <link href="{!! asset('modules/membership/theme/css/pages/wizard/wizard-1.css') !!}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{!! asset('modules/membership/theme/js/pages/crud/forms/validation/biodata-wizard-6.js') !!}" type="text/javascript"></script>
@endsection