@extends('membership::members.layouts.app')

@section('content')

    @include('membership::members.biodata.wizard-warning')
    @include('membership::members.biodata.wizard-step')

    <div class="d-flex flex-column-fluid">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">Data Pribadi</h3>
                        </div>
                        <form action="{!! route('membership.biodata.personal.save') !!}" method="POST" id="form_wizard_1" autocomplete="off">
                            @csrf
                            <div class="card-body">
                                
                                @include('membership::members.biodata.wizard-alert')

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="full_name" class="col-4 col-form-label">Nama Depan <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('full_name', $data->full_name, ['class' => 'form-control text-uppercase', 'id' => 'full_name', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="b_place" class="col-4 col-form-label">Tempat Lahir <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('b_place', isset($data->hasData->b_place) ? $data->hasData->b_place : null, ['class' => 'form-control text-uppercase', 'id' => 'b_place', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="b_date" class="col-4 col-form-label">Tgl. Lahir <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::date('b_date', isset($data->hasData->b_date) ? $data->hasData->b_date : null, ['class' => 'form-control', 'id' => 'b_date', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-4 col-form-label">Jenis Kelamin <span class="text-danger">*</span></label>
                                            <div class="col-8 col-form-label">
                                                <div class="radio-inline">
                                                    <label class="radio">
                                                        <input type="radio" name="gender" value="L" @if(isset($data->hasData->gender) && $data->hasData->gender == 'L') checked @endif /> 
                                                        <span></span> Laki-Laki
                                                    </label>
                                                    <label class="radio">
                                                        <input type="radio" name="gender" value="P" @if(isset($data->hasData->gender) && $data->hasData->gender == 'P') checked @endif />
                                                        <span></span> Perempuan
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="bb" class="col-4 col-form-label">Berat Badan <span class="text-danger">*</span></label>
                                            <div class="col-4">
                                                {!! Form::number('bb', isset($data->hasData->bb) ? $data->hasData->bb : null, ['class' => 'form-control text-right', 'id' => 'bb']) !!}
                                            </div>
                                            <label class="col-2 col-form-label">kg</label>
                                        </div>
                                        <div class="form-group row">
                                            <label for="tb" class="col-4 col-form-label">Tinggi Badan <span class="text-danger">*</span></label>
                                            <div class="col-4">
                                                {!! Form::number('tb', isset($data->hasData->tb) ? $data->hasData->tb : null, ['class' => 'form-control text-right', 'id' => 'tb']) !!}
                                            </div>
                                            <label class="col-2 col-form-label">cm</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="nik" class="col-4 col-form-label">NIK <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('nik', isset($data->hasData->nik) ? $data->hasData->nik : null, ['class' => 'form-control', 'id' => 'nik', 'maxlength' => 16]) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="no_hp" class="col-4 col-form-label">No. HP <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('no_hp', isset($data->hasData->phone) ? $data->hasData->phone : null, ['class' => 'form-control', 'id' => 'no_hp']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="no_wa" class="col-4 col-form-label">No. WA <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('no_wa', isset($data->hasData->wa) ? $data->hasData->wa : null, ['class' => 'form-control', 'id' => 'no_wa']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="marital" class="col-4 col-form-label">Status Perkawinan <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::select('marital', $marital, isset($data->hasData->marital_id) ? $data->hasData->marital_id : null, [ 'class' => 'form-control text-uppercase selectpicker', 'id' => 'marital', 'placeholder' => 'Pilih']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success mr-2">Simpan & Lanjut</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('css')
    <link href="{!! asset('modules/membership/theme/css/pages/wizard/wizard-1.css') !!}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{!! asset('modules/membership/theme/js/pages/crud/forms/validation/biodata-wizard.js') !!}" type="text/javascript"></script>
@endsection