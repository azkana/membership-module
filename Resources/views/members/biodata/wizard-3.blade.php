@extends('membership::members.layouts.app')

@section('content')

    @include('membership::members.biodata.wizard-warning')
    @include('membership::members.biodata.wizard-step')

    <div class="d-flex flex-column-fluid">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">Kontak Darurat</h3>
                        </div>
                        <form action="{!! route('membership.biodata.emergency.save') !!}" method="POST" id="form_wizard_3" autocomplete="off">
                            @csrf
                            <div class="card-body">
                                
                                @include('membership::members.biodata.wizard-alert')
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="name" class="col-3 col-form-label">Nama Lengkap <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="phone" class="col-3 col-form-label">No. Telpon <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone', 'maxlength' => 20, 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="line1" class="col-3 col-form-label">Alamat <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('line1', null, ['class' => 'form-control', 'id' => 'line1', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="line2" class="col-3 col-form-label"></label>
                                            <div class="col-8">
                                                {!! Form::text('line2', null, ['class' => 'form-control', 'id' => 'line2']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="rt" class="col-3 col-form-label">RT <span class="text-danger">*</span></label>
                                            <div class="col-3">
                                                {!! Form::text('rt', null, ['class' => 'form-control', 'id' => 'rt', 'required']) !!}
                                            </div>
                                            <label for="rw" class="col-2 col-form-label">RW <span class="text-danger">*</span></label>
                                            <div class="col-3">
                                                {!! Form::text('rw', null, ['class' => 'form-control', 'id' => 'rw', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="blok" class="col-3 col-form-label">Blok</label>
                                            <div class="col-3">
                                                {!! Form::text('blok', null, ['class' => 'form-control', 'id' => 'blok']) !!}
                                            </div>
                                            <label for="nomor" class="col-2 col-form-label">No.</label>
                                            <div class="col-3">
                                                {!! Form::text('nomor', null, ['class' => 'form-control', 'id' => 'nomor']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="provinsi" class="col-3 col-form-label">Provinsi <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::select('provinsi', $provinsi, null, [ 'class' => 'form-control selectpicker', 'id' => 'provinsi', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="kabupaten" class="col-3 col-form-label">Kabupaten <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::select('kabupaten', [], null, [ 'class' => 'form-control selectpicker', 'id' => 'kabupaten', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="kecamatan" class="col-3 col-form-label">Kecamatan <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::select('kecamatan', [], null, [ 'class' => 'form-control selectpicker', 'id' => 'kecamatan', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="kelurahan" class="col-3 col-form-label">Kelurahan <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::select('kelurahan', [], null, [ 'class' => 'form-control selectpicker', 'id' => 'kelurahan', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="kodepos" class="col-3 col-form-label">Kodepos <span class="text-danger">*</span></label>
                                            <div class="col-4">
                                                {!! Form::text('kodepos', null, ['class' => 'form-control', 'id' => 'kodepos', 'required']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success mr-2">Simpan & Lanjut</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    <link href="{!! asset('modules/membership/theme/css/pages/wizard/wizard-1.css') !!}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{!! asset('modules/membership/theme/js/pages/crud/forms/validation/biodata-wizard-3.js') !!}" type="text/javascript"></script>
    <script>
        $(document).ready(function() {
            var provinsi = $("#provinsi");
            var kabupaten = $("#kabupaten");
            var kecamatan = $("#kecamatan");
            var kelurahan = $("#kelurahan");
            let placeHolder = '<option value="">Pilih</option>';

            provinsi.on('change', function(e) {
                e.preventDefault();
                let provId = e.target.value;
                if(provId !== '') {
                    $.ajax({
                        url: "{!! route('kabupaten.list') !!}",
                        type: "GET",
                        data: { prov_id: provId },
                        success: function (data) {
                            kabupaten.empty().append(placeHolder);
                            $.each(data, function(index, kab) {
                                kabupaten.append('<option value="' + kab.code + '">' + kab.name + '</option>').selectpicker('refresh');
                            });
                            kecamatan.empty().append(placeHolder).selectpicker('refresh');
                            kelurahan.empty().append(placeHolder).selectpicker('refresh');
                        }
                    });
                } else {
                    kabupaten.empty().append(placeHolder).selectpicker('refresh');
                    kecamatan.empty().append(placeHolder).selectpicker('refresh');
                    kelurahan.empty().append(placeHolder).selectpicker('refresh');
                }
            });
            kabupaten.on('change', function(e) {
                e.preventDefault();
                let kabId = e.target.value;
                if(kabId !== '') {
                    $.ajax({
                        url: "{!! route('kecamatan.list') !!}",
                        type: "GET",
                        data: { kab_id: kabId },
                        success: function (data) {
                            kecamatan.empty().append(placeHolder);
                            $.each(data, function(index, kec) {
                                kecamatan.append('<option value="' + kec.code + '">' + kec.name + '</option>').selectpicker('refresh');
                            });
                            kelurahan.empty().append(placeHolder).selectpicker('refresh');
                        }
                    });
                } else {
                    kecamatan.empty().append(placeHolder).selectpicker('refresh');
                    kelurahan.empty().append(placeHolder).selectpicker('refresh');
                }
            });
            kecamatan.on('change', function(e) {
                e.preventDefault();
                let id = e.target.value;
                if(id !== '') {
                    $.ajax({
                        url: "{!! route('kelurahan.list') !!}",
                        type: "GET",
                        data: { kec_id: id },
                        success: function (data) {
                            kelurahan.empty().append(placeHolder);
                            $.each(data, function(index, kel) {
                                kelurahan.append('<option value="' + kel.code + '">' + kel.name + '</option>').selectpicker('refresh');
                            });
                        }
                    });
                } else {
                    kelurahan.empty().append(placeHolder).selectpicker('refresh');
                }
            });
        });
    </script>
@endsection