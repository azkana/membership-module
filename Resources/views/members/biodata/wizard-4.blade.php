@extends('membership::members.layouts.app')

@section('content')

    @include('membership::members.biodata.wizard-warning')
    @include('membership::members.biodata.wizard-step')

    <div class="d-flex flex-column-fluid">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">Pendidikan Teakhir</h3>
                        </div>
                        <form action="{!! route('membership.biodata.education.save') !!}" method="POST" id="form_wizard_4" autocomplete="off">
                            @csrf
                            <div class="card-body">
                                
                                @include('membership::members.biodata.wizard-alert')

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="degree" class="col-4 col-form-label">Tingkat Pendidikan <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::select('degree', $degree, null, [ 'class' => 'form-control selectpicker', 'id' => 'degree', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="institution" class="col-4 col-form-label">Universitas/Sekolah <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('institution', null, ['class' => 'form-control text-uppercase', 'id' => 'institution', 'maxlength' => 50, 'required']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="majors" class="col-4 col-form-label">Jurusan <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('majors', null, ['class' => 'form-control text-uppercase', 'id' => 'majors', 'required']) !!}
                                            </div>
                                        </div>
                                        {{-- <div class="form-group row">
                                            <label for="start_year" class="col-4 col-form-label">Tahun Masuk <span class="text-danger">*</span></label>
                                            <div class="col-4">
                                                {!! Form::text('start_year', null, ['class' => 'form-control', 'id' => 'start_year', 'maxlength' => 4, 'required']) !!}
                                            </div>
                                        </div> --}}
                                        <div class="form-group row">
                                            <label for="graduation" class="col-4 col-form-label">Tahun Lulus</label>
                                            <div class="col-4">
                                                {!! Form::text('graduation', null, ['class' => 'form-control', 'id' => 'graduation', 'maxlength' => 4]) !!}
                                            </div>
                                        </div>
                                        {{-- <div class="form-group row">
                                            <label for="grade" class="col-4 col-form-label">IPK</label>
                                            <div class="col-4">
                                                {!! Form::text('grade', null, ['class' => 'form-control', 'id' => 'grade']) !!}
                                            </div>
                                        </div> --}}
                                        {{-- <div class="form-group row">
                                            <label for="note" class="col-4 col-form-label">Keterangan</label>
                                            <div class="col-8">
                                                {!! Form::textarea('note', null, ['class' => 'form-control', 'id' => 'note', 'rows' => 4]) !!}
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success mr-2">Simpan & Lanjut</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    <link href="{!! asset('modules/membership/theme/css/pages/wizard/wizard-1.css') !!}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{!! asset('modules/membership/theme/js/pages/crud/forms/validation/biodata-wizard-4.js') !!}" type="text/javascript"></script>
    {{-- <script>
        // $(document).ready(function() {
            // var provinsi = $("#provinsi");
            // var kabupaten = $("#kabupaten");
            // var placeHolder = '<option value="">Pilih</option>';
            // provinsi.on('change', function(e) {
            //     e.preventDefault();
            //     let provId = e.target.value;
            //     if(provId !== '') {
            //         $.ajax({
            //             url: "{!! route('kabupaten.list') !!}",
            //             type: "GET",
            //             data: { prov_id: provId },
            //             success: function (data) {
            //                 kabupaten.empty().append(placeHolder);
            //                 $.each(data, function(index, kab) {
            //                     kabupaten.append('<option value="' + kab.code + '">' + kab.name + '</option>').selectpicker('refresh');
            //                 });
            //             }
            //         });
            //     } else {
            //         kabupaten.empty().append(placeHolder).selectpicker('refresh');
            //     }
            // });
            
        // });
    </script> --}}
@endsection