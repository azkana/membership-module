<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="alert alert-warning" role="alert">
            Hai <b>{!! Auth::user()->full_name !!}</b>, 
            Biodata kamu belum lengkap, silahkan lengkapi biodata kamu, agar kami bisa tahu jika ada lowongan yang cocok untuk kamu.
        </div>
    </div>
</div>