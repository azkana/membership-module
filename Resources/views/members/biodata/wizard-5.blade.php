@extends('membership::members.layouts.app')

@section('content')

    @include('membership::members.biodata.wizard-warning')
    @include('membership::members.biodata.wizard-step')

    <div class="d-flex flex-column-fluid">
        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <h3 class="card-title">Pengalaman Kerja</h3>
                        </div>
                        <form action="{!! route('membership.biodata.experience.save') !!}" method="POST" id="form_wizard_5" autocomplete="off">
                            @csrf
                            <div class="card-body">
                                
                                @include('membership::members.biodata.wizard-alert')
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="company" class="col-4 col-form-label">Nama Perusahaan <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('company', null, ['class' => 'form-control text-uppercase', 'id' => 'company', 'maxlength' => 100, 'required', 'autofocus']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="position" class="col-4 col-form-label">Posisi/Jabatan <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::text('position', null, ['class' => 'form-control text-uppercase', 'id' => 'position', 'maxlength' => 50, 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="work_year" class="col-4 col-form-label">Lama Bekerja <span class="text-danger">*</span></label>
                                            <div class="col-4">
                                                {!! Form::select('work_year', $workYear, null, [ 'class' => 'form-control selectpicker', 'id' => 'work_year', 'data-size' => 5, 'placeholder' => 'Pilih']) !!}
                                            </div>
                                            <div class="col-4">
                                                {!! Form::select('work_month', $workMonth, null, [ 'class' => 'form-control selectpicker', 'id' => 'work_month', 'data-size' => 5, 'placeholder' => 'Pilih']) !!}
                                            </div>
                                        </div>
                                        {{-- <div class="form-group row">
                                            <label for="year_start" class="col-4 col-form-label">Tahun Masuk <span class="text-danger">*</span></label>
                                            <div class="col-4">
                                                {!! Form::selectRange('year_start', date('Y'), 2000, null, [ 'class' => 'form-control selectpicker', 'id' => 'year_start', 'data-size' => 5, 'placeholder' => 'Pilih Tahun']) !!}
                                            </div>
                                            <div class="col-4">
                                                {!! Form::selectMonth('month_start', null, [ 'class' => 'form-control selectpicker', 'id' => 'month_start', 'data-size' => 5, 'placeholder' => 'Pilih Bulan']) !!}
                                            </div>
                                        </div> --}}
                                        {{-- <div class="form-group row">
                                            <label for="is_present" class="col-4 col-form-label">Tahun Keluar <span class="text-danger">*</span></label>
                                            <div class="col-8 col-form-label">
                                                <div class="checkbox-inline">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="is_present" id="stillWorking" />
                                                        <span></span>Masih Bekerja
                                                    </label>
                                                </div>
                                            </div>
                                        </div> --}}
                                        {{-- <div class="form-group row" id="group-end">
                                            <label for="year_end" class="col-4 col-form-label"></label>
                                            <div class="col-4">
                                                {!! Form::selectRange('year_end', date('Y'), 2000, null, [ 'class' => 'form-control selectpicker', 'id' => 'year_end', 'data-size' => 5, 'placeholder' => 'Pilih Tahun']) !!}
                                            </div>
                                            <div class="col-4">
                                                {!! Form::selectMonth('month_end', null, [ 'class' => 'form-control selectpicker', 'id' => 'month_end', 'data-size' => 5, 'placeholder' => 'Pilih Bulan']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="salary" class="col-4 col-form-label">Gaji <span class="text-danger">*</span></label>
                                            <div class="col-5">
                                                {!! Form::text('salary', null, ['class' => 'form-control text-right number', 'id' => 'salary', 'required']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="provinsi" class="col-4 col-form-label">Provinsi <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::select('provinsi', $provinsi, null, [ 'class' => 'form-control selectpicker', 'id' => 'provinsi', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="kabupaten" class="col-4 col-form-label">Kabupaten <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::select('kabupaten', [], null, [ 'class' => 'form-control selectpicker', 'id' => 'kabupaten', 'data-size' => 5, 'data-live-search' => 'true', 'placeholder' => 'Pilih']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label for="summary" class="col-4 col-form-label">Summary <span class="text-danger">*</span></label>
                                            <div class="col-8">
                                                {!! Form::textarea('summary', null, ['class' => 'form-control', 'id' => 'summary', 'rows' => 10, 'maxlength' => 1000]) !!}
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-2"></div>
                                    <div class="col-10">
                                        <button type="submit" class="btn btn-success mr-2">Simpan & Lanjut</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    <link href="{!! asset('modules/membership/theme/css/pages/wizard/wizard-1.css') !!}" rel="stylesheet" type="text/css" />
@endsection

@section('js')
    <script src="{!! asset('modules/membership/theme/plugins/jquery.number.min.js') !!}" type="text/javascript"></script>
    <script src="{!! asset('modules/membership/theme/js/pages/crud/forms/validation/biodata-wizard-5.js') !!}" type="text/javascript"></script>
    {{-- <script>
        $(document).ready(function() {
            var stillWorking = $("#stillWorking");
            var groupEnd = $("#group-end");
            var provinsi = $("#provinsi");
            var kabupaten = $("#kabupaten");
            var placeHolder = '<option value="">Pilih</option>';

            stillWorking.on('click', function(e) {
                if(this.checked === true) {
                    groupEnd.addClass('d-none');
                } else {
                    groupEnd.removeClass('d-none');
                }
            });

            $('.number').number(true, 0);

            provinsi.on('change', function(e) {
                e.preventDefault();
                let provId = e.target.value;
                if(provId !== '') {
                    $.ajax({
                        url: "{!! route('kabupaten.list') !!}",
                        type: "GET",
                        data: { prov_id: provId },
                        success: function (data) {
                            kabupaten.empty().append(placeHolder);
                            $.each(data, function(index, kab) {
                                kabupaten.append('<option value="' + kab.code + '">' + kab.name + '</option>').selectpicker('refresh');
                            });
                        }
                    });
                } else {
                    kabupaten.empty().append(placeHolder).selectpicker('refresh');
                }
            });
            
        });
    </script> --}}
@endsection