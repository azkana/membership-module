@extends('membership::members.layouts.app')

@section('content')
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="card card-custom gutter-b">
            <div class="card-body ribbon ribbon-left">
                @if($data->membership == 'FREE')
                    <div class="ribbon-target bg-success" style="top: 10px; left: -2px;">{!! $data->membership !!}</div>
                @else
                    <div class="ribbon-target bg-warning" style="top: 10px; left: -2px;">{!! $data->membership !!}</div>
                @endif
                <div class="d-flex mb-9">
                    <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                        <div class="symbol symbol-50 symbol-lg-120">
                            @if(isset($data->hasData->attc_foto) && !empty($data->hasData->attc_foto))
                                <img src="{!! asset('modules/membership/theme/media/users/300_6.jpg') !!}" alt="image" />
                            @else
                                <img src="{!! asset('modules/membership/theme/media/users/blank.png') !!}" alt="image" />
                            @endif
                        </div>
                        <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                            <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                        </div>
                    </div>
                    <div class="flex-grow-1">
                        <div class="d-flex justify-content-between flex-wrap mt-1">
                            <div class="d-flex mr-3">
                                <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{!! $data->full_name !!}</a>
                                <a href="#">
                                    <i class="flaticon2-correct text-success font-size-h5"></i>
                                </a>
                            </div>
                            {{-- <div class="my-lg-0 my-3">
                                <a href="#" class="btn btn-sm btn-light-success font-weight-bolder text-uppercase mr-3">Panggil</a>
                                <a href="#" class="btn btn-sm btn-info font-weight-bolder text-uppercase">hire</a>
                            </div> --}}
                        </div>
                        <div class="d-flex flex-wrap justify-content-between mt-1">
                            <div class="d-flex flex-column flex-grow-1 pr-8">
                                <div class="d-flex flex-wrap mb-4">
                                    @if(!empty($data->no_member))
                                    <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                        <i class="flaticon2-new-email mr-2 font-size-lg"></i>{!! $data->no_member !!}
                                    </a>
                                    @endif
                                    <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <i class="flaticon2-new-email mr-2 font-size-lg"></i>{!! $data->email !!}</a>
                                    @if(isset($data->hasData))
                                    <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                    <i class="flaticon2-calendar-3 mr-2 font-size-lg"></i>{!! $data->hasData->phone !!}</a>
                                    @endif
                                    @if(isset($alamatDom))
                                    <a href="#" class="text-dark-50 text-hover-primary font-weight-bold">
                                        <i class="flaticon2-placeholder mr-2 font-size-lg"></i>
                                        {!! $alamatDom->getProvinsi->name !!}
                                    </a>
                                    @endif
                                </div>
                                <span class="font-weight-bold text-dark-50">I distinguish three main text objectives could be merely to inform people.</span>
                                <span class="font-weight-bold text-dark-50">A second could be persuade people.You want people to bay objective</span>
                            </div>
                            <div class="d-flex align-items-center w-25 flex-fill float-right mt-lg-12 mt-8">
                                <span class="font-weight-bold text-dark-75">Biodata</span>
                                <div class="progress progress-xs mx-3 w-100">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 63%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="font-weight-bolder text-dark">78%</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="separator separator-solid"></div>
            </div>
        </div>
    </div>
</div>

@if ($data->doc_status == 'NONE')
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-body d-flex p-0">
                            <div class="flex-grow-1 p-8 card-rounded bgi-no-repeat d-flex align-items-center" style="background-color: #c5e4ec; background-position: right bottom; background-size: auto 100%; background-image: url({!! asset('modules/membership/theme/media/svg/humans/custom-3.svg') !!})">
                                <div class="row">
                                    <div class="col-12 col-xl-1"></div>
                                    <div class="col-12 col-xl-11">
                                        <h4 class="text-warning font-weight-bolder">Lengkapi Data Kamu</h4>
                                        <p class="text-dark-50 my-5 font-size-xl font-weight-bold">Segera lengkapi data kamu ya, biar kamu bisa mendapatkan informasi pekerjaan yang akurat</p>
                                        <a href="{!! route('membership.biodata.personal') !!}" class="btn btn-warning font-weight-bold py-2 px-6">Lengkapi sekarang</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if($data->doc_status == 'PENDING')
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-2">
                <div class="card card-custom card-stretch gutter-b">
                    <div class="card-body d-flex p-0">
                        <div class="flex-grow-1 p-8 card-rounded bgi-no-repeat d-flex align-items-center" style="background-color: #ffffff; background-position: right bottom; background-size: auto 100%; background-image: url({!! asset('modules/membership/theme/media/svg/humans/custom-3.svg') !!})">
                            <div class="row">
                                <div class="col-12 col-xl-12">
                                    <h4 class="text-warning font-weight-bolder">Data kamu sedang diproses</h4>
                                    <p class="text-dark-50 my-5 font-size-xl font-weight-bold">Data kamu sedang dalam proses verifikasi oleh team DKU</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

@if($data->membership == 'FREE' AND $data->doc_status == 'APPROVED')
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-2">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-body d-flex p-0">
                            <div class="flex-grow-1 p-8 card-rounded bgi-no-repeat d-flex align-items-center" style="background-color: #FFF4DE; background-position: right bottom; background-size: auto 100%; background-image: url({!! asset('modules/membership/theme/media/svg/humans/custom-3.svg') !!})">
                                <div class="row">
                                    <div class="col-12 col-xl-1"></div>
                                    <div class="col-12 col-xl-11">
                                        <h4 class="text-danger font-weight-bolder">Upgrade Keangotaan kamu menjadi PRO</h4>
                                        <p class="text-dark-50 my-5 font-size-xl font-weight-bold">Masa aktif keanggotaan sampai mendapatkan pekerjaan</p>
                                        <a href="{!! route('membership.payment.order') !!}" class="btn btn-danger font-weight-bold py-2 px-6">Upgrade sekarang</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

@if($data->doc_status == 'APPROVED')
<div class="d-flex flex-column-fluid">
    <div class="container">
        <div class="d-flex flex-row">
            <div class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px" id="kt_profile_aside">
                <div class="card card-custom card-stretch">
                    <div class="card-body pt-4">
                        {{-- begin::Nav --}}
                        <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                            <div class="navi-item mb-2">
                                <a href="#" class="navi-link py-4 tab_data" id="tab_account">
                                    <span class="navi-icon mr-2">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M12.9336061,16.072447 L19.36,10.9564761 L19.5181585,10.8312381 C20.1676248,10.3169571 20.2772143,9.3735535 19.7629333,8.72408713 C19.6917232,8.63415859 19.6104327,8.55269514 19.5206557,8.48129411 L12.9336854,3.24257445 C12.3871201,2.80788259 11.6128799,2.80788259 11.0663146,3.24257445 L4.47482784,8.48488609 C3.82645598,9.00054628 3.71887192,9.94418071 4.23453211,10.5925526 C4.30500305,10.6811601 4.38527899,10.7615046 4.47382636,10.8320511 L4.63,10.9564761 L11.0659024,16.0730648 C11.6126744,16.5077525 12.3871218,16.5074963 12.9336061,16.072447 Z" fill="#000000" fill-rule="nonzero" />
                                                    <path d="M11.0563554,18.6706981 L5.33593024,14.122919 C4.94553994,13.8125559 4.37746707,13.8774308 4.06710397,14.2678211 C4.06471678,14.2708238 4.06234874,14.2738418 4.06,14.2768747 L4.06,14.2768747 C3.75257288,14.6738539 3.82516916,15.244888 4.22214834,15.5523151 C4.22358765,15.5534297 4.2250303,15.55454 4.22647627,15.555646 L11.0872776,20.8031356 C11.6250734,21.2144692 12.371757,21.2145375 12.909628,20.8033023 L19.7677785,15.559828 C20.1693192,15.2528257 20.2459576,14.6784381 19.9389553,14.2768974 C19.9376429,14.2751809 19.9363245,14.2734691 19.935,14.2717619 L19.935,14.2717619 C19.6266937,13.8743807 19.0546209,13.8021712 18.6572397,14.1104775 C18.654352,14.112718 18.6514778,14.1149757 18.6486172,14.1172508 L12.9235044,18.6705218 C12.377022,19.1051477 11.6029199,19.1052208 11.0563554,18.6706981 Z" fill="#000000" opacity="0.3" />
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="navi-text font-size-lg">Akun</span>
                                </a>
                            </div>
                            <div class="navi-item mb-2">
                                <a href="#" class="navi-link py-4 tab_data" id="tab_personal">
                                    <span class="navi-icon mr-2">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="navi-text font-size-lg">Data Pribadi</span>
                                </a>
                            </div>
                            <div class="navi-item mb-2">
                                <a href="#" class="navi-link py-4 tab_data" id="tab_education">
                                    <span class="navi-icon mr-2">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="navi-text font-size-lg">Pendidikan</span>
                                </a>
                            </div>
                            <div class="navi-item mb-2">
                                <a href="#" class="navi-link py-4 tab_data" id="tab_experiences">
                                    <span class="navi-icon mr-2">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="navi-text font-size-lg">Pengalaman Kerja</span>
                                </a>
                            </div>
                            <div class="navi-item mb-2">
                                <a href="#" class="navi-link py-4 tab_data" id="tab_address">
                                    <span class="navi-icon mr-2">
                                        <span class="svg-icon">
                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                    <polygon points="0 0 24 0 24 24 0 24" />
                                                    <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                                    <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                                                </g>
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="navi-text font-size-lg">Alamat</span>
                                </a>
                            </div>
                        </div>
                        {{-- end::Nav --}}
                    </div>
                </div>
            </div>
            
            @include('membership::members.profile.default')
            
        </div>
    </div>
</div>
@endif

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            var id = '{!! Auth::user()->id !!}'
            checkBiodata(id);
            var tabContent = $('#data_content');
            var tabAccount = $('#tab_account');
            var tabPersonal = $('#tab_personal');
            var tabEducation = $('#tab_education');
            var tabExp = $('#tab_experiences');
            var tabAddress = $('#tab_address');

            tabAccount.on('click', function(e) {
                e.preventDefault();
                resetTabActive();
                tabAccount.addClass('active');
                $.ajax({
                    type: 'get',
                    url: '{!! route("membership.account") !!}',
                    data: {id: id},
                    beforeSend: function() {
                        KTApp.block(tabContent, {});
                    },
                    success: function(data) {
                        tabContent.html(data);
                    },
                    error: function(xhr) {
                        toastr.error("Gagal mengambil data.");
                    }
                });
            });
            tabPersonal.on('click', function(e) {
                e.preventDefault();
                resetTabActive();
                tabPersonal.addClass('active');
                $.ajax({
                    type: 'get',
                    url: '{!! route("membership.personal") !!}',
                    data: {id: id},
                    beforeSend: function() {
                        KTApp.block(tabContent, {});
                    },
                    success: function(data) {
                        tabContent.html(data);
                    },
                    error: function(xhr) {
                        toastr.error("Gagal mengambil data.");
                    }
                });
            });
            tabEducation.on('click', function(e) {
                e.preventDefault();
                resetTabActive();
                tabEducation.addClass('active');
                $.ajax({
                    type: 'get',
                    url: '{!! route("membership.education") !!}',
                    data: {id: id},
                    beforeSend: function() {
                        KTApp.block(tabContent, {});
                    },
                    success: function(data) {
                        tabContent.html(data);
                    },
                    error: function(xhr) {
                        toastr.error("Gagal mengambil data.");
                    }
                });
            });
            tabExp.on('click', function(e) {
                e.preventDefault();
                resetTabActive();
                tabExp.addClass('active');
                $.ajax({
                    type: 'get',
                    url: '{!! route("membership.experiences") !!}',
                    data: {id: id},
                    beforeSend: function() {
                        KTApp.block(tabContent, {});
                    },
                    success: function(data) {
                        tabContent.html(data);
                    },
                    error: function(xhr) {
                        toastr.error("Gagal mengambil data.");
                    }
                });
            });
            tabAddress.on('click', function(e) {
                e.preventDefault();
                resetTabActive();
                tabAddress.addClass('active');
                $.ajax({
                    type: 'get',
                    url: '{!! route("membership.address") !!}',
                    data: {id: id},
                    beforeSend: function() {
                        KTApp.block(tabContent, {});
                    },
                    success: function(data) {
                        tabContent.html(data);
                    },
                    error: function(xhr) {
                        toastr.error("Gagal mengambil data.");
                    }
                });
            });

            $('#nik').maxlength({
                warningClass: "label label-warning label-rounded label-inline",
                limitReachedClass: "label label-success label-rounded label-inline"
            });

            function resetTabActive() {
                $('.tab_data').removeClass('active');
            }

            function checkBiodata(id) {
                $.ajax({
                    type: 'get',
                    url: '{!! route("membership.biodata.check") !!}',
                    data: {id: id},
                    success: function(data) {
                        // console.log(data);
                        // if(data.exists === "false") {
                        //     window.location.href = data.redirect
                        // }
                    },
                    // error: function(xhr) {
                    //     toastr.error("Gagal mengambil data.");
                    // }
                });
            }
        });
    </script>
@endsection