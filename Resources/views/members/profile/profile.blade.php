<div class="flex-row-fluid ml-lg-8" id="data_content">
    <div class="card card-custom card-stretch">
        <div class="card-header py-3">
            <div class="card-title align-items-start flex-column">
                <h3 class="card-label font-weight-bolder text-dark">Informasi Akun</h3>
                <span class="text-muted font-weight-bold font-size-sm mt-1">Pengaturan Akun</span>
            </div>
            <div class="card-toolbar">
                <button type="button" class="btn btn-success mr-2">Simpan Perubahan</button>
                <button type="reset" class="btn btn-secondary">Batal</button>
            </div>
        </div>
        <form class="form">
            <div class="card-body">
                <div class="row">
                    <label class="col-xl-3"></label>
                    <div class="col-lg-9 col-xl-6">
                        <h5 class="font-weight-bold mb-6">Account:</h5>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                    <div class="col-lg-9 col-xl-6">
                        <div class="input-group input-group-md input-group-solid">
                            <input type="text" class="form-control form-control-md form-control-solid" value="{!! Auth::user()->email !!}" placeholder="Email" />
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-xl-3 col-lg-3 col-form-label">Email Verified at</label>
                    <div class="col-lg-9 col-xl-6">
                        <div class="input-group input-group-md input-group-solid">
                            <input type="text" class="form-control form-control-md form-control-solid" value="{!! dateFormatDmyHi(Auth::user()->verified_at) !!}" disabled />
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>