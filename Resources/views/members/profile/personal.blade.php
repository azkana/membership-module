<div class="flex-row-fluid ml-lg-8" id="data_content">
    <form class="form card card-custom card-stretch" autocomplete="off">
        <div class="card-header py-3">
            <div class="card-title align-items-start flex-column">
                <h3 class="card-label font-weight-bolder text-dark">Data Pribadi</h3>
                <span class="text-muted font-weight-bold font-size-sm mt-1">Pengaturan Data Pribadi</span>
            </div>
            <div class="card-toolbar"></div>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Nama Lengkap</label>
                <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" value="{!! $data->full_name !!}" disabled />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">NIK</label>
                <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" id="nik" value="{!! !is_null($data->hasData) ? $data->hasData->nik : null !!}" maxlength="16" disabled />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">KK</label>
                <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" id="nik" value="{!! !is_null($data->hasData) ? $data->hasData->kk : null !!}" maxlength="16" disabled />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Tempat Lahir</label>
                <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" id="nik" value="{!! !is_null($data->hasData) ? $data->hasData->b_place : null !!}" disabled />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Tgl. Lahir</label>
                <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" id="nik" value="{!! !is_null($data->hasData) ? dateFormatDmy($data->hasData->b_date) : null !!}" disabled />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Jenis Kelamin</label>
                <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" id="nik" value="{!! !is_null($data->hasData) ? $data->hasData->gender : null !!}" disabled />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Tinggi & Berat Badan</label>
                <div class="col-lg-3 col-xl-3">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" id="nik" value="{!! !is_null($data->hasData) ? $data->hasData->tb . ' cm' : null !!}" disabled />
                    </div>
                </div>
                <div class="col-lg-3 col-xl-3">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" id="nik" value="{!! !is_null($data->hasData) ? $data->hasData->bb . ' kg' : null !!}" disabled />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Agama</label>
                <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" id="nik" value="{!! !is_null($data->hasData) ? $data->hasData->getAgama->name : null !!}" disabled />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">Status Perkawinan</label>
                <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" id="nik" value="{!! !is_null($data->hasData) ? $data->hasData->getMarital->name : null !!}" disabled />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-xl-3 col-lg-3 col-form-label">NPWP</label>
                <div class="col-lg-9 col-xl-6">
                    <div class="input-group input-group-md input-group-solid">
                        <input type="text" class="form-control form-control-md form-control-solid" id="nik" value="{!! !is_null($data->hasData) ? $data->hasData->npwp : null !!}" disabled />
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>