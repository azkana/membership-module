<div class="flex-row-fluid ml-lg-8" id="data_content">
    <form class="form card card-custom card-stretch">
        <div class="card-header py-3">
            <div class="card-title align-items-start flex-column">
                <h3 class="card-label font-weight-bolder text-dark">Alamat</h3>
                <span class="text-muted font-weight-bold font-size-sm mt-1">Pengaturan Alamat</span>
            </div>
            <div class="card-toolbar"></div>
        </div>
        <div class="card-body" style="min-height: 300px">
            <div class="table-responsive">
                <table class="table table-head-custom table-vertical-center table-head-bg table-borderless">
                    <thead>
                        <tr class="text-left">
                            <th style="min-width: 50px" class="pl-7">
                                <span class="text-dark-75">Jenis</span>
                            </th>
                            <th style="min-width: 120px">Nama</th>
                            <th style="min-width: 100px">Telpon</th>
                            <th style="min-width: 200px">Alamat</th>
                            <th style="min-width: 100px">Kabupaten</th>
                            <th style="min-width: 100px">Provinsi</th>
                            <th style="min-width: 50px">Kodepos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data->hasAddress as $row)
                        <tr>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->type !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->name !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->phone !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm text-capitalize">
                                    {!! $row->line1 !!}
                                    {!! $row->line2 !!}
                                    RT. {!! $row->rt !!}/{!! $row->rw !!}
                                    {!! capitalize($row->getKelurahan->name) !!},
                                    {!! capitalize($row->getKecamatan->name) !!}
                                </span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->getKabupaten->name !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->getProvinsi->name !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->zipcode !!}</span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>