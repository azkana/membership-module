<div class="flex-row-fluid ml-lg-8" id="data_content">
    <form class="form card card-custom card-stretch">
        <div class="card-header py-3">
            <div class="card-title align-items-start flex-column">
                <h3 class="card-label font-weight-bolder text-dark">Pendidikan</h3>
                <span class="text-muted font-weight-bold font-size-sm mt-1">Pengaturan Pendidikan</span>
            </div>
            <div class="card-toolbar"></div>
        </div>
        <div class="card-body" style="min-height: 300px">
            <div class="table-responsive">
                <table class="table table-head-custom table-vertical-center table-head-bg table-borderless">
                    <thead>
                        <tr class="text-left">
                            <th style="min-width: 150px" class="pl-7">
                                <span class="text-dark-75">Jenjang</span>
                            </th>
                            <th style="min-width: 120px">Institusi</th>
                            <th style="min-width: 100px">Jurusan</th>
                            <th style="min-width: 100px">Tahun</th>
                            <th style="min-width: 100px">IPK</th>
                            <th style="min-width: 100px">Lokasi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data->hasEducation as $row)
                        <tr>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->degree !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->institution !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->majors !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">
                                    {!! $row->start_year !!} - 
                                    {!! $row->on_progress == 0 ? $row->graduation : __('Sekarang') !!}
                                </span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->grade !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->getProvinsi->name !!}</span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</div>