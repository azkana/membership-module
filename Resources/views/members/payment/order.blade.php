@extends('membership::members.layouts.app')

@section('content')

    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="row">
                <div class="col-xl-4">
                    <div class="card card-custom gutter-b card-stretch">
                        <div class="card-header border-0 pt-5">
                            <div class="card-title">
                                <div class="card-label">
                                    <div class="font-weight-bolder">Manfaat Menjadi Member PRO</div>
                                    <div class="font-size-sm text-muted mt-2"></div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body d-flex flex-column">
                            <div class="flex-grow-1">
                                <div class="d-flex align-items-center justify-content-between mb-10">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="symbol symbol-50 symbol-light mr-3 flex-shrink-0">
                                            <div class="symbol-label">
                                                <img src="{!! asset('modules/membership/theme/media/svg/misc/006-plurk.svg') !!}" alt="" class="h-50" />
                                            </div>
                                        </div>
                                        <div>
                                            <a href="#" class="font-size-h6 text-dark-75 text-hover-primary font-weight-bolder">Informasi Lowongan Pekerjaan</a>
                                            {{-- <div class="font-size-sm text-muted font-weight-bold mt-1"></div> --}}
                                        </div>
                                    </div>
                                    {{-- <div class="label label-light label-inline font-weight-bold text-dark-50 py-4 px-3 font-size-base"></div> --}}
                                </div>
                                <div class="d-flex align-items-center justify-content-between mb-10">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="symbol symbol-50 symbol-light mr-3 flex-shrink-0">
                                            <div class="symbol-label">
                                                <img src="{!! asset('modules/membership/theme/media/svg/misc/015-telegram.svg') !!}" alt="" class="h-50" />
                                            </div>
                                        </div>
                                        <div>
                                            <a href="#" class="font-size-h6 text-dark-75 text-hover-primary font-weight-bolder">Pemanggilan Test</a>
                                            {{-- <div class="font-size-sm text-muted font-weight-bold mt-1">PitStop Emails</div> --}}
                                        </div>
                                    </div>
                                    {{-- <div class="label label-light label-inline font-weight-bold text-dark-50 py-4 px-3 font-size-base">+60$</div> --}}
                                </div>
                                <div class="d-flex align-items-center justify-content-between mb-10">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="symbol symbol-50 symbol-light mr-3 flex-shrink-0">
                                            <div class="symbol-label">
                                                <img src="{!! asset('modules/membership/theme/media/svg/misc/015-telegram.svg') !!}" alt="" class="h-50" />
                                            </div>
                                        </div>
                                        <div>
                                            <a href="#" class="font-size-h6 text-dark-75 text-hover-primary font-weight-bolder">Membership berlaku sampai mendapat pekerjaan</a>
                                            {{-- <div class="font-size-sm text-muted font-weight-bold mt-1">PitStop Emails</div> --}}
                                        </div>
                                    </div>
                                    {{-- <div class="label label-light label-inline font-weight-bold text-dark-50 py-4 px-3 font-size-base">+60$</div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-8">
                    <div class="card card-custom card-stretch gutter-b">
                        <div class="card-header pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Rincian Biaya Member PRO</span>
                                <span class="text-muted mt-3 font-weight-bold font-size-sm">Lebih dari 10.000 orang sudah menjadi member PRO</span>
                            </h3>
                            <div class="card-toolbar">
                                {{-- <ul class="nav nav-pills nav-pills-sm nav-dark-75">
                                    <li class="nav-item">
                                        <a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_1_1">Month</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_1_2">Week</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link py-2 px-4 active" data-toggle="tab" href="#kt_tab_pane_1_3">Day</a>
                                    </li>
                                </ul> --}}
                            </div>
                        </div>
                        <div class="card-body pt-2 pb-0 mt-n3">
                            <div class="table-responsive">
                                <table class="table table-borderless table-vertical-center">
                                    <thead>
                                        <tr>
                                            <th class="p-0 min-w-200px"></th>
                                            <th class="p-0 min-w-100px"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $kodeUnik = generateUniqueNumber(3);
                                            $memberAmount = isset($orderData->amount) ? $orderData->amount : $memberTypePro->amount;
                                            $memberKodeUnik = isset($orderData->amount_uniq) ? $orderData->amount_uniq : $kodeUnik;
                                            $total = $memberAmount + $memberKodeUnik;
                                        @endphp
                                        <tr>
                                            <td class="py-6 pl-0">
                                                <a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Biaya Membership</a>
                                                <span class="text-muted font-weight-bold d-block">Berlaku sampai mendapat pekerjaan</span>
                                            </td>
                                            <td class="text-right pr-0">
                                                <span class="text-dark-75 font-weight-bolder font-size-lg">{!! numberFormat($memberAmount) !!}</span>
                                                <input type="hidden" id="amount" value="{!! $memberAmount !!}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="py-6 pl-0">
                                                <a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Kode Unik</a>
                                                <span class="text-muted font-weight-bold d-block">untuk memudahkan pencarian data pembayaran</span>
                                            </td>
                                            <td class="text-right pr-0">
                                                <span class="text-dark-75 font-weight-bolder font-size-lg">{!! $memberKodeUnik !!}</span>
                                                <input type="hidden" id="kode_unik" value="{!! $memberKodeUnik !!}" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="py-6 pl-0">
                                                <a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Biaya Layanan</a>
                                                <span class="text-muted font-weight-bold d-block">biaya dari payment gateway</span>
                                            </td>
                                            <td class="text-right pr-0">
                                                <span class="text-dark-75 font-weight-bolder font-size-lg">0</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="py-6 pl-0">
                                                <a href="#" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">Total</a>
                                                <span class="text-muted font-weight-bold d-block">Total biaya yang harus dibayarkan</span>
                                            </td>
                                            <td class="text-right pr-0">
                                                <span class="text-dark-75 font-weight-bolder font-size-lg">{!! numberFormat($total) !!}</span>
                                                <input type="hidden" id="total" value="{!! $total !!}" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <button type="button" id="pay_now" class="btn btn-primary font-weight-bold">Setuju & Bayar Sekarang</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function() {
            var btnPayNow = $("#pay_now");
            var amount = $("#amount");
            var kodeUnik = $("#kode_unik");
            var total = $("#total");
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            btnPayNow.on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    headers: {
                        'x-csrf-token': CSRF_TOKEN,
                    },
                    url: "{!! route('membership.payment.order.save') !!}",
                    type: "POST",
                    data: { 
                        amount: amount.val(),
                        kode_unik: kodeUnik.val(),
                        total: total.val()
                    },
                    success: function (data) {
                        window.location.href = data.redirect
                    }
                });
            });
        });
    </script>
@endsection