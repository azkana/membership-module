<div class="flex-row-fluid ml-lg-8" id="data_content">
    <div class="card card-custom card-stretch">
        <div class="card-header py-3">
            <div class="card-title align-items-start flex-column">
                <h3 class="card-label font-weight-bolder text-dark">Pemanggilan</h3>
                <span class="text-muted font-weight-bold font-size-sm mt-1"></span>
            </div>
            <div class="card-toolbar"></div>
        </div>
        <div class="card-body" style="min-height: 300px">
            <div class="table-responsive">
                <table class="table table-head-custom table-vertical-center table-head-bg table-borderless">
                    <thead>
                        <tr class="text-left">
                            <th style="min-width: 150px" class="pl-7">
                                <span class="text-dark-75">Tanggal Kirim</span>
                            </th>
                            <th style="min-width: 120px">Batas Waktu Konfirmasi</th>
                            <th style="min-width: 100px">Status</th>
                            <th style="min-width: 100px">Keterangan</th>
                            <th class="text-right" style="min-width: 80px">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $row)
                        <tr>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! dateFormatDmyHi($row->created_at) !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! dateFormatDmyHi($row->expired_at) !!}</span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">
                                    {!! in_array($row->status, ['SENT', 'READ']) ? 'WAITING' : $row->status !!}
                                </span>
                            </td>
                            <td>
                                <span class="text-dark-75 d-block font-size-sm">{!! $row->note !!}</span>
                            </td>
                            <td class="pr-0 text-right">
                                @if(!in_array($row->status, ['CONFIRMED', 'REJECTED']))
                                <button class="btn btn-light-success font-weight-bolder font-size-sm" data-toggle="modal" data-target="#staticBackdrop">Konfirmasi</button>
                                @endif
                            </td>
                            <div class="modal fade" id="staticBackdrop" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Konfirmasi Pemanggilan</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <i aria-hidden="true" class="ki ki-close"></i>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <span>Apakah kamu yakin ingin melakukan konfirmasi pemanggilan ? Jika ya Silahkan klik tombol Konfirmasi, jika tidak bersedia silahkan klik tombol Tolak dan isi alasan kamu.</span>
                                                    <br><br>
                                                    <div class="form-group">
                                                        <label>Alasan</label>
                                                        <textarea name="note" id="note" class="form-control form-control-md" maxlength="200" rows="5"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" onclick="confirmNow('{!! $row->id !!}')" class="btn btn-primary font-weight-bold">Konfirmasi</button>
                                            <button type="button" onclick="rejectNow('{!! $row->id !!}')" class="btn btn-danger font-weight-bold">Tolak</button>
                                            <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    function confirmNow(id) {
        var note = $("#note");
        var csrf = $('meta[name="csrf-token"]').attr('content');
        
        $.ajax({
            type: "POST",
            url: "{!! route('membership.rec.pemanggilan.konfirmasi') !!}",
            data: {
                id: id,
                note: note.val(),
                _token: csrf
            },
            success: function(data) {
                toastr.success(data.message);
                $("#staticBackdrop").modal('hide');
                $('#tab_pemanggilan').trigger( "click" );
            },
            error: function(xhr) {
                toastr.error(xhr.message);
            }
        });
    }
    function rejectNow(id) {
        var note = $("#note");
        var csrf = $('meta[name="csrf-token"]').attr('content');
        
        $.ajax({
            type: "POST",
            url: "{!! route('membership.rec.pemanggilan.reject') !!}",
            data: {
                id: id,
                note: note.val(),
                _token: csrf
            },
            success: function(data) {
                toastr.success(data.message);
                $("#staticBackdrop").modal('hide');
                $('#tab_pemanggilan').trigger( "click" );
            },
            error: function(xhr) {
                toastr.error(xhr.message);
            }
        });
    }
</script>