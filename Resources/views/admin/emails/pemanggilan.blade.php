@extends('beautymail::templates.minty')

@section('content')

    @include('beautymail::templates.minty.contentStart')

    <tr>
        <td class="title">
            Hi, <strong>{!! $data['full_name'] !!}</strong>
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td class="paragraph">
            Selamat kamu terpilih untuk melakukan test, jika kamu bersedia silahkan klik tombol konfirmasi sekarang di bawah ini.
            Terima kasih.
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>
    <tr>
        <td>
            @include('beautymail::templates.minty.button', ['text' => 'Konfirmasi Sekarang', 'link' => route('membership.recruitment')])
            <img src="{!! route('email.read', 'id='.$data['log_id']) !!}" width="1" height="1" />
        </td>
    </tr>
    <tr>
        <td width="100%" height="25"></td>
    </tr>

    @include('beautymail::templates.minty.contentEnd')

@stop