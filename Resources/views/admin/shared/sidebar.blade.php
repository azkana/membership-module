@can('new-members')
<li class="treeview @if($adminActiveMenu == 'members') active @endif">
    <a href="#">
        <i class="fa fa-users"></i>
        <span>Membership</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
            <span class="label label-danger pull-right">
                New
            </span>
        </span>
    </a>
    <ul class="treeview-menu">
        <li class="@if($adminActiveSubMenu == 'dashboard') active @endif">
            <a href="{!! route('membership.dashboard') !!}">
                <i class="fa fa-circle-o"></i> Dashboard
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'verifikasi') active @endif">
            <a href="{!! route('membership.verifikasi') !!}">
                <i class="fa fa-circle-o"></i> Verifikasi Data
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'order') active @endif">
            <a href="{!! route('membership.order.index') !!}">
                <i class="fa fa-circle-o"></i> Payment
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'index') active @endif">
            <a href="{!! route('membership.index') !!}">
                <i class="fa fa-circle-o"></i> Members
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'pemanggilan') active @endif">
            <a href="{!! route('membership.pemanggilan') !!}">
                <i class="fa fa-circle-o"></i> Pemanggilan
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'pemanggilan-konfirmasi') active @endif">
            <a href="{!! route('membership.pemanggilan.konfirmasi') !!}">
                <i class="fa fa-circle-o"></i> Konfirmasi Pemanggilan
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'psikotest') active @endif">
            <a href="{!! route('membership.psikotest') !!}">
                <i class="fa fa-circle-o"></i> Psikotest
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'interview') active @endif">
            <a href="{!! route('membership.interview') !!}">
                <i class="fa fa-circle-o"></i> Interview
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'mcu') active @endif">
            <a href="{!! route('membership.mcu') !!}">
                <i class="fa fa-circle-o"></i> Medical Check-Up
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'joined') active @endif">
            <a href="{!! route('membership.joined') !!}">
                <i class="fa fa-circle-o"></i> Joined
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'reports') active @endif">
            <a href="{!! route('membership.report') !!}">
                <i class="fa fa-circle-o"></i> Reports
                <span class="pull-right-container"></span>
            </a>
        </li>
        <li class="@if($adminActiveSubMenu == 'dashboard') active @endif">
            <a href="{!! route('membership.dashboard') !!}">
                <i class="fa fa-circle-o"></i> Settings
                <span class="pull-right-container"></span>
            </a>
        </li>
    </ul>
</li>
@endcan