@extends('membership::layouts.master')

@section('content')
    
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body" style="min-height: 520px">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="start_date" class="small">Start Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('start_date', date('d-m-Y'), [ 'class' => 'form-control datepicker input-sm', 'id' => 'start_date']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="end_date" class="small">End Date</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('end_date', date('d-m-Y'), [ 'class' => 'form-control datepicker input-sm', 'id' => 'end_date']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-xs-6">
                                    <div class="form-group">
                                        <label for="button" class="small"></label>
                                        <div class="input-group">
                                            <button class="btn btn-sm btn-success" style="margin-top: 4.5px" id="btn_search">
                                                <i class="fa fa-search"></i> Cari
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"></div>
                    </div>
                    <hr>
                    <div class="row" style="margin-bottom: 10px">
                        <div class="col-md-12">
                            <button class="btn btn-xs btn-success hide" id="pemanggilan">
                                <i class="fa fa-forward"></i>
                                Proses Pemanggilan
                            </button>
                        </div>
                    </div>
                    <div class="divider"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table id="grid-members" class="table table-bordered table-striped" style="width:100%;">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th class="text-center" style="width: 2%">#</th>
                                            <th class="text-center" style="width: 5%">No. Member</th>
                                            <th class="text-center">NIK</th>
                                            <th class="text-center">Nama Lengkap</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center" style="width: 100px">Tgl. Daftar</th>
                                            <th class="text-center" style="width: 5%">
                                                <i class="fa fa-navicon"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="detail">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div id="content"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('css')
    <style>
        .datepicker table tr td.disabled{
            color:red;
            cursor: not-allowed;
        }
        .datepicker table tr td.today{
            background: orange;
        }
        .datepicker table tr td.active{
            font-weight: bold;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var btnPemanggilan = $('#pemanggilan');
            var startDate = $('#start_date');
            var endDate = $('#end_date');
            var btnSearch = $('#btn_search');
            if(localStorage.getItem('mbr_start_date') !== null) {
                startDate.val(localStorage.getItem('mbr_start_date'));
            }
            if(localStorage.getItem('mbr_end_date') !== null) {
                endDate.val(localStorage.getItem('mbr_end_date'));
            }
            $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true,
                weekStart: 1,
                endDate: '+1d',
                language: 'id'
            });

            var table = $('#grid-members').DataTable({
                processing: true,
                serverSide: true,
                responsive: false,
                PaginationType: "two_button",
                paginate:true,
                deferRender: true,
                keys: false,
                scrollX: true,
                dom: '<"row"<"col-md-4"l><"col-md-4 text-center"B><"col-md-4"f>>' + 'rtip',
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                buttons: [
                    {
                        extend: 'selectAll',
                        text: 'Select All',
                        className: "btn-sm",
                    },
                    {
                        extend:'selectNone',
                        text: 'Select None',
                        className: "btn-sm",
                    },
                    {
                        text: '<i class="fa fa-refresh"></i> Refresh',
                        className: "btn-sm",
                        action: function(e, dt, node, config) {
                            dt.ajax.reload();
                            btnPemanggilan.addClass('hide');
                        }
                    }
                ],
                ajax: { 
                    url: "{!! route('membership.index.data') !!}",
                    pages: 10,
                    data: function(d) {
                        d.start_date = startDate.val();
                        d.end_date = endDate.val();
                    }
                },
                columns: [
                    {data: 'checkboxes', name: 'checkboxes', orderable: false, searchable: false},
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', className: "small", orderable: false, searchable: false},
                    {data: 'no_member', name: 'no_member', className: "small"},
                    {data: 'nik', name: 'nik', className: "small"},
                    {data: 'full_name', name: 'full_name', className: "small"},
                    {data: 'email', name: 'email', className: "small"},
                    {data: 'created_at', name: 'created_at', className: "small"},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
                columnDefs: [
                    {
                        targets: 0,
                        className: 'select-checkbox'
                    },
                    {targets: 6, render: $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD-MM-YYYY HH:mm:ss' )},
                ],
                order: [
                    [6, 'desc']
                ],
                language: {
                    processing: "<p class='text-center'>Loading...</p>"
                },
            });

            table.on( 'select', function ( e, dt, type, indexes ) {
                var count = table.rows( { selected: true } ).count();
                if ( count > 0 ) {
                    btnPemanggilan.removeClass('hide');
                }
            } );

            table.on( 'deselect', function ( e, dt, type, indexes ) {
                var count = table.rows( { selected: true } ).count();
                if ( count === 0 ) {
                    btnPemanggilan.addClass('hide');
                }
            } );

            btnSearch.on('click', function(e) {
                e.preventDefault();
                getDataMember();
                localStorage.setItem('mbr_start_date', startDate.val());
                localStorage.setItem('mbr_end_date', endDate.val());
            });

            btnPemanggilan.on('click', function(e) {
                e.preventDefault();
                var countRows = table.rows( { selected: true } ).data().count();
                swal({
                    title: 'Konfirmasi',
                    text: 'Apakah kamu ingin melakukan pemanggilan pada ' + countRows + ' data ini?',
                    buttons: ["Batal", "Ya"],
                    dangerMode: true,
                    closeOnClickOutside: false,
                }).then(function(value) {
                    if(value) {
                        var selectedRows = table.rows( { selected: true } ).data().to$();
                        var selectedId = Array();
                        $.each(selectedRows, function(i, e) {
                            selectedId.push(e.id);
                        });
                        $.ajax({
                            url: "{!! route('membership.pemanggilan.proses.bulk') !!}",
                            type: "post",
                            data: {
                                _token: "{!! csrf_token() !!}",
                                id: selectedId,
                            },
                            success: function(res, stat, xhr) {
                                swal({
                                    text: res.message,
                                    icon: 'success',
                                    button: 'close',
                                    closeOnClickOutside: false,
                                });
                                table.draw();
                                btnPemanggilan.addClass('hide');
                            },
                            error: function(err) {
                                swal({
                                    text: err,
                                    icon: 'error',
                                    button: 'close',
                                    closeOnClickOutside: false,
                                });
                            }
                        });
                    }
                })
            });

            $('.dataTables_scrollBody').css('height', '350px');
            
            function getDataMember() {
                $.ajax({
                    url: "{!! route('membership.index.data') !!}",
                    type: "GET",
                    data: { 
                        start_date: startDate.val(),
                        end_date: endDate.val()
                    },
                    success: function(data) {
                        table.ajax.reload();
                    }
                });
            }
        });
        function getDetail(id) {
            $.ajax({
                type: "GET",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{!! route('membership.show') !!}",
                data: {id: id},
                success: function(data) {
                    $('#detail').modal('toggle');
                    $('#content').html(data);
                }
            });
        }
    </script>
@endsection