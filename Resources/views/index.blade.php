@extends('membership::layouts.master')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-solid">
                <div class="box-header"></div>
                <div class="box-body" style="min-height: 520px">
                    <blockquote>
                        <p>Welcome, <b>{!! Auth::user()->name !!}</b></p>
                        <small>Your Roles <cite title="">{!! Auth::user()->roles->pluck('name')->implode(',') !!}</cite></small>
                        <small>Member Since <cite title="">{!! date('d F Y', strtotime(Auth::user()->created_at)) !!}</cite></small>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
@endsection
