// Class definition
var KTFormControls = function () {
	// Private functions
	var _initFormWizard3 = function() {
		FormValidation.formValidation(
			document.getElementById('form_wizard_5'),
			{
				fields: {
					company: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							stringLength: {
								max:100,
								message: 'maksimal 100 karakter'
							}
						}
					},
					position: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							stringLength: {
								max:50,
								message: 'maksimal 50 karakter'
							}
						}
					},
					work_year: {
						validators: {
							notEmpty: {
								message: 'harus dipilih'
							},
						}
					},
					work_month: {
						validators: {
							notEmpty: {
								message: 'harus dipilih'
							},
						}
					},
					// year_start: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'harus dipilih'
					// 		},
					// 	}
					// },
					// month_start: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'harus dipilih'
					// 		},
					// 	}
					// },
					// year_end: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'harus dipilih'
					// 		}
					// 	}
					// },
					// month_end: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'harus dipilih'
					// 		}
					// 	}
					// },
					// provinsi: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'harus dipilih'
					// 		},
					// 	}
					// },
					// kabupaten: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'harus dipilih'
					// 		},
					// 	}
					// },
					// summary: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'harus diisi'
					// 		},
					// 		stringLength: {
					// 			min: 100,
					// 			max: 1000,
					// 			message: 'minimal 100 karakter, maksimal 1000 karakter'
					// 		}
					// 	}
					// }
				},

				plugins: { //Learn more: https://formvalidation.io/guide/plugins
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap(),
					// Validate fields when clicking the Submit button
					submitButton: new FormValidation.plugins.SubmitButton(),
            		// Submit the form when all fields are valid
            		defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
					excluded: new FormValidation.plugins.Excluded(),
				}
			}
		);
	}

	return {
		// public functions
		init: function() {
			_initFormWizard3();
		}
	};
}();

jQuery(document).ready(function() {
	KTFormControls.init();
});
