// Class definition
var KTFormControls = function () {
	// Private functions
	var _initFormWizard = function() {
		FormValidation.formValidation(
			document.getElementById('form_wizard_6'),
			{
				fields: {
					foto: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							file: {
								extension: 'jpeg,jpg,png',
								type: 'image/jpeg,image/png',
								maxSize: 2097152,   // 2048 * 1024
								message: 'Jenis file tidak sesuai atau ukuran terlalu besar'
							},
						}
					},
					ktp: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							file: {
								extension: 'jpeg,jpg,png',
								type: 'image/jpeg,image/png',
								maxSize: 2097152,   // 2048 * 1024
								message: 'Jenis file tidak sesuai atau ukuran terlalu besar'
							},
						}
					},
				},

				plugins: { //Learn more: https://formvalidation.io/guide/plugins
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap(),
					// Validate fields when clicking the Submit button
					submitButton: new FormValidation.plugins.SubmitButton(),
            		// Submit the form when all fields are valid
            		defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
					excluded: new FormValidation.plugins.Excluded(),
				}
			}
		);
	}

	return {
		// public functions
		init: function() {
			_initFormWizard();
		}
	};
}();

jQuery(document).ready(function() {
	KTFormControls.init();
});
