// Class definition
var KTFormControls = function () {
	// Private functions
	var _initFormWizard1 = function () {
		FormValidation.formValidation(
			document.getElementById('form_wizard_1'),
			{
				fields: {
					full_name: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
						}
					},

					b_place: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
						}
					},

					b_date: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
						}
					},

					gender: {
						validators: {
							choice: {
								min:1,
								message: 'harus dipilih'
							}
						}
					},

					tb: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							digits: {
								message: 'harus angka'
							}
						}
					},

					bb: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							digits: {
								message: 'harus angka'
							}
						}
					},

					nik: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							digits: {
								message: 'harus angka'
							},
							stringLength: {
								min:16,
								max:16,
								message: 'NIK harus 16 digit'
							}
						}
					},

					no_hp: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							digits: {
								message: 'harus angka'
							}
						}
					},

					no_wa: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							digits: {
								message: 'harus angka'
							}
						}
					},

					marital: {
						validators: {
							notEmpty: {
								message: 'harus dipilih'
							}
						}
					}
				},

				plugins: { //Learn more: https://formvalidation.io/guide/plugins
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap(),
					// Validate fields when clicking the Submit button
					submitButton: new FormValidation.plugins.SubmitButton(),
            		// Submit the form when all fields are valid
            		defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				}
			}
		);
	}

	return {
		// public functions
		init: function() {
			_initFormWizard1();
		}
	};
}();

jQuery(document).ready(function() {
	KTFormControls.init();
});
