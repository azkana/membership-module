// Class definition
var KTFormControls = function () {
	// Private functions
	var _initFormWizard3 = function() {
		FormValidation.formValidation(
			document.getElementById('form_wizard_3'),
			{
				fields: {
					name: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							stringLength: {
								max:100,
								message: 'maksimal 100 karakter'
							}
						}
					},
					phone: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							digits: {
								message: 'harus angka'
							},
							stringLength: {
								max:20,
								message: 'maksimal 20 karakter'
							}
						}
					},
					line1: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							stringLength: {
								max:100,
								message: 'maksimal 100 karakter'
							}
						}
					},
					line2: {
						validators: {
							stringLength: {
								max:100,
								message: 'maksimal 100 karakter'
							}
						}
					},
					rt: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							digits: {
								message: 'harus angka'
							}
						}
					},
					rw: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							digits: {
								message: 'harus angka'
							}
						}
					},
					provinsi: {
						validators: {
							notEmpty: {
								message: 'harus dipilih'
							},
						}
					},
					kabupaten: {
						validators: {
							notEmpty: {
								message: 'harus dipilih'
							},
						}
					},
					kecamatan: {
						validators: {
							notEmpty: {
								message: 'harus dipilih'
							},
						}
					},
					kelurahan: {
						validators: {
							notEmpty: {
								message: 'harus dipilih'
							},
						}
					},
					kodepos: {
						validators: {
							digits: {
								message: 'harus angka'
							},
							stringLength: {
								min:5,
								max:5,
								message: 'Kodepos harus 5 digit'
							}
						}
					},
				},

				plugins: { //Learn more: https://formvalidation.io/guide/plugins
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap(),
					// Validate fields when clicking the Submit button
					submitButton: new FormValidation.plugins.SubmitButton(),
            		// Submit the form when all fields are valid
            		defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				}
			}
		);
	}

	return {
		// public functions
		init: function() {
			_initFormWizard3();
		}
	};
}();

jQuery(document).ready(function() {
	KTFormControls.init();
});
