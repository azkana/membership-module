// Class definition
var KTFormControls = function () {
	// Private functions
	var _initFormWizard3 = function() {
		FormValidation.formValidation(
			document.getElementById('form_wizard_4'),
			{
				fields: {
					degree: {
						validators: {
							notEmpty: {
								message: 'harus dipilih'
							},
						}
					},
					institution: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							stringLength: {
								max:50,
								message: 'maksimal 50 karakter'
							}
						}
					},
					majors: {
						validators: {
							notEmpty: {
								message: 'harus diisi'
							},
							stringLength: {
								max:50,
								message: 'maksimal 50 karakter'
							}
						}
					},
					// provinsi: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'harus dipilih'
					// 		},
					// 	}
					// },
					// kabupaten: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'harus dipilih'
					// 		},
					// 	}
					// },
					// start_year: {
					// 	validators: {
					// 		notEmpty: {
					// 			message: 'harus diisi'
					// 		},
					// 		digits: {
					// 			message: 'harus angka'
					// 		},
					// 		stringLength: {
					// 			min: 4,
					// 			max: 4,
					// 			message: 'harus 4 karakter'
					// 		}
					// 	}
					// },
					graduation: {
						validators: {
							digits: {
								message: 'harus angka'
							},
							stringLength: {
								min: 4,
								max: 4,
								message: 'harus 4 karakter'
							}
						}
					},
					// grade: {
					// 	validators: {
					// 		numeric: {
					// 			message: 'harus angka. Desimal menggunakan tanda titik ( . ).',
					// 			decimalSeparator: '.'
					// 		},
					// 		stringLength: {
					// 			max: 5,
					// 			message: 'maksimal 5 karakter'
					// 		}
					// 	}
					// },
					// note: {
					// 	validators: {
					// 		stringLength: {
					// 			max: 100,
					// 			message: 'maksimal 100 karakter'
					// 		}
					// 	}
					// }
				},

				plugins: { //Learn more: https://formvalidation.io/guide/plugins
					trigger: new FormValidation.plugins.Trigger(),
					// Bootstrap Framework Integration
					bootstrap: new FormValidation.plugins.Bootstrap(),
					// Validate fields when clicking the Submit button
					submitButton: new FormValidation.plugins.SubmitButton(),
            		// Submit the form when all fields are valid
            		defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
				}
			}
		);
	}

	return {
		// public functions
		init: function() {
			_initFormWizard3();
		}
	};
}();

jQuery(document).ready(function() {
	KTFormControls.init();
});
