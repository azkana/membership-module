"use strict";

var KTDropzone = function () {

    var attachmentDocument = function () {
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $('.dropzone').dropzone({
            headers: {
                'x-csrf-token': CSRF_TOKEN,
            },
            url: "upload",
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 5,
            addRemoveLinks: true,
            acceptedFiles: ".jpg,.png,.jpeg",
            autoProcessQueue: false,
            uploadMultiple: false,

            init: function() {

                var dropZone = this;

                $('#submit').on('click', function(e){
                    e.preventDefault();
                    dropZone.processQueue();
                });

                this.on('sending', function(file, xhr, formData) {
                    var data = $('#form_wizard_6').serializeArray();
                    $.each(data, function(key, el) {
                        formData.append(el.name, el.value);
                    });
                });
            },
        });
    }

    return {
        init: function() {
            attachmentDocument();
        }
    }
}();

KTUtil.ready(function() {
    KTDropzone.init();
});