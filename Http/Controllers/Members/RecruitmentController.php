<?php

namespace Modules\Membership\Http\Controllers\Members;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Membership\Entities\Members;
use Modules\Membership\Entities\Pemanggilan;

class RecruitmentController extends Controller
{
    protected $pageTitle;

    public function __construct()
    {
        $this->pageTitle = 'Recruitment';
    }

    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        $id = Auth::user()->id;
        $params['data']         = $data = Members::findOrFail($id);
        $params['alamatDom']    = $data->hasAddress->where('type', 'dom')->first();
        return view('membership::members.recruitment.index', $params);
    }

    public function pemanggilan(Request $request)
    {
        $userID = Auth::user()->id;
        $params['data'] = Pemanggilan::where('member_id', $userID)
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('membership::members.recruitment.pemanggilan', $params);
    }

    public function pemanggilanForm(Request $request)
    {
        $params['data'] = Pemanggilan::findOrFail($request->id);
        return view('membership::members.recruitment.modals.confirmation', $params);
    }

    public function pemanggilanConfirm(Request $request)
    {
        try {
            $note = $request->note;
            $id = $request->id;
            Pemanggilan::where('id', $id)->update([
                'status' => Pemanggilan::STATUS_CONFIRMED,
                'confirmed_at' => Carbon::now(),
                'note' => $note
            ]);

            return response()->json([
                'code' => 200,
                'message' => 'Berhasil melakukan konfirmasi'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 200,
                'message' => 'Gagal melakukan konfirmasi' . $e
            ]);
        }
    }

    public function pemanggilanReject(Request $request)
    {
        try {
            $note = $request->note;
            $id = $request->id;
            $pemanggilan = Pemanggilan::findOrFail('id', $id);
            $pemanggilan->update([
                'status' => Pemanggilan::STATUS_REJECTED,
                'confirmed_at' => Carbon::now(),
                'note' => $note
            ]);
            Members::findOrFail($pemanggilan->member_id)
                ->update([
                    'rec_status' => Members::REC_STATUS_AVAILABLE
                ]);

            return response()->json([
                'code' => 200,
                'message' => 'Berhasil menolak panggilan'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'code' => 200,
                'message' => 'Gagal menolak panggilan' . $e
            ]);
        }
    }
}
