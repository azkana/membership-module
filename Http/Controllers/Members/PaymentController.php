<?php

namespace Modules\Membership\Http\Controllers\Members;

use Carbon\Carbon;
use Midtrans\Snap;
use GuzzleHttp\Client;
use Midtrans\Notification;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\Logs\Email;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Modules\MemberRegistration\Entities\Member;
use Modules\Membership\Entities\Members;
use Modules\Membership\Entities\MemberType;
use Modules\Membership\Entities\MemberOrder;
use Modules\Membership\Entities\MemberPayment;
use Modules\Membership\Jobs\MembershipPaymentSuccessJob;

class PaymentController extends Controller
{
    protected $pageTitle;

    public function __construct()
    {
        $this->pageTitle = 'Upgrade Member';
    }

    public function index()
    {
        $client = new Client();
        try {
            $res = $client->request('GET', 'http://jobsdku.loc/test', []);
            $data = json_decode($res->getBody()->getContents());
            dd($data);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }

    public function order()
    {
        $params['pageTitle'] = $this->pageTitle;
        $params['memberTypePro']= MemberType::where('code', 'PRO')->firstOrFail();
        $params['orderData']    = MemberOrder::where('member_id', Auth::user()->id)
            ->where('status', MemberOrder::UNPAID)
            ->where('due_date', '>=', date('Y-m-d H:i:s'))
            ->orderBy('created_at', 'DESC')
            ->limit(1)
            ->first();
        return view('membership::members.payment.order', $params);
    }

    public function orderSave(Request $request)
    {
        try {
            // dd($request->all());
            $memberID = Auth::user()->id;
            /* Check Pending Order */
            $memberOrder = MemberOrder::where('member_id', $memberID)
                ->where('status', MemberOrder::UNPAID)
                ->where('due_date', '>=', date('Y-m-d H:i:s'))
                ->orderBy('created_at', 'DESC');
            
            if($memberOrder->count() > 0) {
                $memberOrderDetail = $memberOrder->first();
                return response()->json([
                    'code' => 200,
                    'redirect' => $memberOrderDetail->payment_url
                ]);
            }

            $data['code']       = MemberOrder::_generateOrderNumber();
            $data['amount']     = $request->amount;
            $data['amount_uniq']= $request->kode_unik;
            $data['grand_total']= $request->total;
            $data['order_date'] = Carbon::now();
            $data['status']     = MemberOrder::CREATED;
            $data['due_date']   = Carbon::now()->addDays(1);
            $data['payment_status'] = MemberOrder::UNPAID;
            $data['member_id']  = $memberID;
            $order = MemberOrder::create($data);
            $snap = $this->_generatePaymentToken($order);

            return response()->json([
                'code' => 200,
                'redirect' => $snap
            ]);
        } catch (\Exception $e) {
            return $e;
        }
    }

    private function _generatePaymentToken($order)
    {
        $this->initPaymentGateway();

        $member = Members::findOrFail(Auth::user()->id);

        $customerDetails = [
            'first_name' => $member->first_name,
            'last_name' => $member->last_name,
            'email' => $member->email,
            'phone' => $member->hasData->phone
        ];

        $params = [
            'enabled_payments' => MemberPayment::PAYMENT_CHANNELS,
            'transaction_details' => [
                'order_id' => $order['code'],
                'gross_amount' => $order['grand_total'],
            ],
            'customer_details' => $customerDetails,
            'expiry' => [
                'start_time' => date('Y-m-d H:i:s T'),
                'unit' => MemberPayment::EXPIRY_UNIT,
                'duration' => MemberPayment::EXPIRY_DURATION
            ]
        ];
        $snap = Snap::createTransaction($params);

        if($snap->token) {
            $order->payment_token = $snap->token;
            $order->payment_url = $snap->redirect_url;
            $order->save();
        }
        return $snap->redirect_url;
    }
    public function notification(Request $request)
    {
        $payload = $request->getContent();
        $notification = json_decode($payload);

        $validSignature = hash("sha512", $notification->order_id . $notification->status_code . $notification->gross_amount . config('payment.midtrans_server_key'));
        // dd($validSignature);
        if($notification->signature_key != $validSignature) {
            return response([
                'message' => 'Invalid Signature'
            ], 403);
        }

        $this->initPaymentGateway();
        $statusCode = null;

        $paymentNotification = new Notification();
        $order = MemberOrder::where('code', $paymentNotification->order_id)->first();

        if($order->isPaid()) {
            return response(['message' => 'This order has been paid.'], 422);
        }

        $transaction = $paymentNotification->transaction_status;
        $type = $paymentNotification->payment_type;
        $orderId = $paymentNotification->order_id;
        $fraud = $paymentNotification->fraud_status;

        $vaNumber = null;
        $vendorName = null;
        if(!empty($paymentNotification->va_numbers[0])) {
            $vaNumber = $paymentNotification->va_numbers[0]->va_number;
            $vendorName = $paymentNotification->va_numbers[0]->bank;
        }

        $paymentStatus = null;
        if($transaction == 'capture') {
            if($type == 'credit_card') {
                if($fraud == 'challenge') {
                    $paymentStatus = MemberPayment::CHALLENGE;
                } else {
                    $paymentStatus = MemberPayment::SUCCESS;
                }
            }
        } else if($transaction == 'settlement') {
            $paymentStatus = MemberPayment::SETTLEMENT;
        } else if($transaction == 'pending') {
            $paymentStatus = MemberPayment::PENDING;
        } else if($transaction == 'deny') {
            $paymentStatus = MemberPayment::DENY;
        } else if($transaction == 'expire') {
            $paymentStatus = MemberPayment::EXPIRE;
        } else if($transaction == 'cancel') {
            $paymentStatus = MemberPayment::CANCEL;
        }

        $paymentParams = [
            'order_id' => $order->id,
            'number' => MemberPayment::_generatePaymentNumber(),
            'amount' => $paymentNotification->gross_amount,
            'method' => 'midtrans',
            'status' => $paymentStatus,
            'token' => $paymentNotification->transaction_id,
            'payload' => $payload,
            'payment_type' => $paymentNotification->payment_type,
            'va_number' => $vaNumber,
            'vendor_name' => $vendorName,
            'biller_code' => $paymentNotification->biller_code,
            'bill_key' => $paymentNotification->bill_key
        ];

        $payment= MemberPayment::create($paymentParams);
        $member = Members::findOrFail($order->member_id);

        if($paymentStatus && $payment) {
            \DB::transaction(
                function () use ($order, $payment, $member) {
                    if(in_array($payment->status, [MemberPayment::SUCCESS, MemberPayment::SETTLEMENT])) {
                        $order->status = MemberOrder::CONFIRMED;
                        $order->payment_status = MemberOrder::PAID;
                        $order->save();
                        $member->no_member = Members::_generateMemberNumber();
                        $member->membership = MemberType::MEMBER_PRO;
                        $member->rec_status = Members::REC_STATUS_AVAILABLE;
                        $member->save();

                        $logEmail = Email::create([
                            'email_from'    => config('mail.from.address'),
                            'email_to'      => $member->email,
                            'email_subject' => 'Jobsdku - Pembayaran Berhasil diverifikasi',
                            'email_status'  => 'Q',
                            'module'        => 'membership',
                            'queue_at'      => Carbon::now()->format('Y-m-d H:i:s')
                        ]);

                        $dataEmail['full_name'] = $member->full_name;
                        $dataEmail['email']     = $member->email;
                        $dataEmail['log_id']  = $logEmail->id;
                        
                        MembershipPaymentSuccessJob::dispatch($dataEmail)
                            ->delay(now()->addMinutes(1));
                    }
                }
            );
        }

        $message = 'Payment status is ' . $paymentStatus;

        $response = [
            'code' => 200,
            'message' => $message
        ];

        return response($response, 200);
    }
    public function completed(Request $request)
    {
        $code = $request->query('order_id');
        $order = MemberOrder::where('code', $code)->firstOrFail();

        if($order->payment_status == MemberOrder::UNPAID) {
            return redirect('members/payment/failed?order_id=' . $code);
        }

        Session::flash('success', 'Thank you for completing the payment process.');

        return redirect()->route('membership.home');
    }
    public function failed(Request $request)
    {
        $code = $request->query('order_id');
        $order = MemberOrder::where('code', $code)->firstOrFail();

        Session::flash('error', 'Sorry, we couldn\'t process your payment.');

        return redirect()->route('membership.home');
    }
    public function unfinished(Request $request)
    {
        $code = $request->query('order_id');
        $order = MemberOrder::where('code', $code)->firstOrFail();

        Session::flash('error', 'Sorry, we couldn\'t process your payment.');

        return redirect()->route('membership.home');
    }
    public function testSendEmail()
    {
        $member = Members::findOrFail(Auth::user()->id);
        $logEmail = Email::create([
            'email_from'    => config('mail.from.address'),
            'email_to'      => $member->email,
            'email_subject' => 'Jobsdku - Pembayaran Berhasil diverifikasi',
            'email_status'  => 'Q',
            'module'        => 'membership',
            'queue_at'      => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $dataEmail['full_name'] = $member->full_name;
        $dataEmail['email']     = $member->email;
        $dataEmail['log_id']  = $logEmail->id;
        
        MembershipPaymentSuccessJob::dispatch($dataEmail)
            ->delay(now()->addMinutes(1));
    }
}
