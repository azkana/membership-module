<?php

namespace Modules\Membership\Http\Controllers\Members\Auth;

use App\Models\Logs\Email;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;
use Modules\Membership\Entities\Members;
use Illuminate\Support\Facades\Validator;
use Modules\Membership\Entities\MemberType;
use Modules\Membership\Jobs\MembershipEmailVerificationJob;

class RegisterController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:member');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'fullname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    public function register(Request $request)
    {
        try {
            $fullname = $request->fullname;
            $nameArray = explode(' ', $fullname);
            $firstName = $nameArray[0];
            $lastName = $this->setLastName($nameArray);
            $email = $request->email;
            $password = $request->password;

            /** Check Email */
            $checkEmail = $this->checkEmailMember($email);
            if($checkEmail != 0) {
                return response()->json([
                    'code' => false,
                    'message' => "Email sudah terdaftar, Silahkan menggunakan email yang lain."
                ], 200);
            }

            $data = array(
                'first_name' => $firstName,
                'last_name' => $lastName,
                'email' => $email,
                'password' => $password
            );

            $newMember = $this->create($data);

            $dataEmail['full_name'] = $fullname;
            $dataEmail['email']     = $email;

            /** Log Email */
            $logEmail = Email::create([
                'email_from'    => config('mail.from.address'),
                'email_to'      => $email,
                'email_subject' => 'Jobsdku - Verifikasi Email',
                'email_status'  => 'Q',
                'module'        => 'membership',
                'queue_at'      => date('Y-m-d H:i:s')
            ]);

            $dataEmail['log_id']  = $logEmail->id;
            $dataEmail['member_id'] = $newMember->id;

            /** Send Email Confirmation */
            MembershipEmailVerificationJob::dispatch($dataEmail)
                ->delay(now()->addMinutes(1));
            
            return response()->json([
                'code' => true,
                'message' => 'Pendaftaran berhasil.'
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
        
    }

    public function registerSuccess()
    {
        return view('membership::members.auth.register-success');
    }

    public function verifyEmail($id)
    {
        $member = Members::where('id', $id);
        $memberEmailVerifiedCheck = $member->where('verified_at', '!=', null)->get();
        $memberDetail = Members::findOrFail($id);
        $status = 'true';
        
        try {
            if($memberEmailVerifiedCheck->count() == 1) {
                $status = 'false';
                return redirect()->route('membership.email.verify.success', [$id, $status]);
            } else {
                $memberDetail->update([
                    'verified_at' => date('Y-m-d H:i:s'),
                    'is_active' => true
                ]);
                return redirect()->route('membership.email.verify.success', [$id, $status]);
            }
        } catch (\Exception $e) {
            $status = 'error';
            return redirect()->route('membership.email.verify.success', [$id, $status]);
        }
    }

    public function verifyEmailSuccess($id, $status)
    {
        if($status == 'false') {
            $message = 'Email sudah diverifikasi.';
        } elseif($status == 'true') {
            $message = 'Email kamu berhasil diverifikasi.<br><br><a href="'. route('membership.login') .'" class="btn btn-outline-primary" style="width: 100px">Login</a>' ;
        } else {
            $message = 'Email tidak berhasil diverifikasi.';
        }
        $params['data'] = Members::findOrFail($id);
        $params['message'] = $message;
        $params['status'] = $status;
        return view('membership::members.auth.email-verify', $params);
    }

    public function create(array $data)
    {
        return Members::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_active' => false,
            'membership' => MemberType::MEMBER_FREE,
            'doc_status' => Members::DOC_STATUS_NONE
        ]);
    }

    private function checkEmailMember($email)
    {
        $emailIsExists = Members::where('email', $email)->get();
        return $emailIsExists->count();
    }

    public static function setLastName($name = array())
    {
        $lastName = '';
        if(isset($name[1])) {
            $lastName = $name[1];
        }

        if(isset($name[2])) {
            $lastName = $name[1] . ' ' . $name[2];
        }

        if(isset($name[3])) {
            $lastName = $name[1] . ' ' . $name[2] . ' ' . $name[3];
        }

        if(isset($name[4])) {
            $lastName = $name[1] . ' ' . $name[2] . ' ' . $name[3] . ' ' . $name[4];
        }

        return $lastName;
    }
}
