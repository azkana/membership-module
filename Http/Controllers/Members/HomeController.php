<?php

namespace Modules\Membership\Http\Controllers\Members;

use Image;
use Storage;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Models\Master\Agama;
use App\Models\Master\Marital;
use App\Models\Master\Provinsi;
use App\Models\Master\Nationality;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Membership\Entities\DocumentApproval;
use Modules\Membership\Entities\Members;
use Modules\Membership\Entities\MemberData;
use Modules\Membership\Entities\MemberAddress;
use Modules\Membership\Entities\MemberEducation;
use Modules\Membership\Entities\MemberExperience;
use Modules\Membership\Http\Controllers\Members\Auth\RegisterController;

class HomeController extends Controller
{
    protected $pageTitle;
    protected $disk;

    public function __construct()
    {
        $this->pageTitle = 'Dashboard';
        $this->disk = config('membership.disk');
    }

    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        $id = Auth::user()->id;
        $params['data']         = $data = Members::findOrFail($id);
        $params['alamatDom']    = $data->hasAddress->where('type', 'dom')->first();
        return view('membership::members.index', $params);
    }

    public function membersIndex()
    {
        return redirect()->route('membership.login');
    }

    public function account(Request $request)
    {
        $params['data'] = Members::findOrFail($request->id);
        return view('membership::members.profile.account', $params);
    }

    public function personal(Request $request)
    {
        $params['data'] = Members::findOrFail($request->id);
        return view('membership::members.profile.personal', $params);
    }

    public function education(Request $request)
    {
        $params['data'] = Members::findOrFail($request->id);
        return view('membership::members.profile.education', $params);
    }

    public function experiences(Request $request)
    {
        $params['data'] = Members::findOrFail($request->id);
        return view('membership::members.profile.experiences', $params);
    }

    public function address(Request $request)
    {
        $params['data'] = Members::findOrFail($request->id);
        return view('membership::members.profile.address', $params);
    }

    /** Biodata Wizard */
    public function biodatacheck(Request $request)
    {
        $data = Members::findOrFail($request->id);
        $result = 'true';

        if(count($data->hasData) == 0) {
            $result = array(
                'code' => 200,
                'exists' => 'false',
                'redirect' => route('membership.biodata.personal')
            );
        } else
        if(count($data->hasAddress) == 0) {
            $result = array(
                'code' => 200,
                'exists' => 'false',
                'redirect' => route('membership.biodata.address')
            );
        } else
        if(count($data->hasEducation) == 0) {
            $result = array(
                'code' => 200,
                'exists' => 'false',
                'redirect' => route('membership.biodata.step4')
            );
        } else
        if(count($data->hasExperience) == 0) {
            $result = array(
                'code' => 200,
                'exists' => 'false',
                'redirect' => route('membership.biodata.step5')
            );
        }

        return $result;
    }
    // Deprecated
    public function biodataWizard(Request $request)
    {
        $params['pageTitle']    = 'Pengisian Biodata';
        $params['data']         = $data = Members::findOrFail(Auth::user()->id);
        $params['religion']     = Agama::getReligionDropdownList(true);
        $params['provinsi']     = Provinsi::getProvinsiDropdownList(true);
        $params['nationality']  = Nationality::getListForDropdownList();
        $params['marital']      = Marital::getMaritalForDropdownList();
        return view('membership::members.biodata.wizard', $params);
    }

    public function wizardPersonal()
    {
        $params['pageTitle']    = 'Pengisian Biodata';
        $params['state']        = 'data-pribadi';
        $params['data']         = Members::findOrFail(Auth::user()->id);
        $params['marital']      = Marital::getMaritalForDropdownList(true);
        $params['nationality']  = Nationality::getListForDropdownList();
        $params['provinsi']     = Provinsi::getProvinsiDropdownList(true);
        return view('membership::members.biodata.wizard-1', $params);
    }

    public function wizardPersonalSave(Request $request)
    {
        try {
            $member = Members::findOrFail(Auth::user()->id);
            $fullName = uppercase($request->full_name);
            $nameArray = explode(' ', $fullName);
            $firstName = $nameArray[0];
            $dataMember['first_name']   = $firstName;
            $dataMember['last_name']    = RegisterController::setLastName($nameArray);
            $data['nik']    = $request->nik;
            $data['phone']  = $request->no_hp;
            $data['wa']     = $request->no_wa;
            $data['b_place']= uppercase($request->b_place);
            $data['b_date'] = $request->b_date;
            $data['gender'] = $request->gender;
            $data['tb']     = $request->tb;
            $data['bb']     = $request->bb;
            $data['marital_id']     = $request->marital;
            $data['member_id']      = $member->id;
            
            $member->update($dataMember);

            $memberData = MemberData::where('member_id', Auth::user()->id);

            if($memberData->count() == 0) {
                MemberData::create($data);
            } else {
                $memberData->update($data);
            }

            return redirect()->route('membership.biodata.address');
        } catch (\Exception $e) {
            return redirect(route('membership.biodata.personal'))
				->with('error', trans('message.error_save'));
        }
    }

    public function wizardAddress()
    {
        $params['pageTitle']    = 'Pengisian Biodata';
        $params['data']         = Members::findOrFail(Auth::user()->id);
        $params['state']        = 'alamat';
        $params['provinsi']     = Provinsi::getProvinsiDropdownList(true);
        return view('membership::members.biodata.wizard-2', $params);
    }

    public function wizardAddressSave(Request $request)
    {
        try {
            $memberID       = Auth::user()->id;
            $member         = Members::findOrFail($memberID);
            /* Data Alamat Sesuai KTP */
            $data['type']   = 'ktp';
            $data['line1']  = uppercase($request->line1);
            $data['rt']     = $request->rt;
            $data['rw']     = $request->rw;
            $data['blok']   = uppercase($request->blok);
            $data['no']     = uppercase($request->nomor);
            $data['kel_id'] = $request->kelurahan;
            $data['kec_id'] = $request->kecamatan;
            $data['kab_id'] = $request->kabupaten;
            $data['prov_id']= $request->provinsi;
            $data['zipcode']= $request->kodepos;
            $data['member_id']  = $memberID;

            $memberAddressKtp = MemberAddress::where('member_id', $memberID)
                ->where('type', 'ktp');

            if($memberAddressKtp->count() == 0) {
                MemberAddress::create($data);
            } else {
                $memberAddressKtp->update($data);
            }
            /* Data Alamat Sesuai Domisili */
            $dataDom['type']   = 'dom';
            $dataDom['line1']  = uppercase($request->dom_line1);
            $dataDom['rt']     = $request->dom_rt;
            $dataDom['rw']     = $request->dom_rw;
            $dataDom['blok']   = uppercase($request->dom_blok);
            $dataDom['no']     = uppercase($request->dom_nomor);
            $dataDom['kel_id'] = $request->dom_kelurahan;
            $dataDom['kec_id'] = $request->dom_kecamatan;
            $dataDom['kab_id'] = $request->dom_kabupaten;
            $dataDom['prov_id']= $request->dom_provinsi;
            $dataDom['zipcode']= $request->dom_kodepos;
            $dataDom['member_id']  = $memberID;

            $memberAddressDomisili = MemberAddress::where('member_id', Auth::user()->id)
                ->where('type', 'dom');

            if($memberAddressDomisili->count() == 0) {
                MemberAddress::create($dataDom);
            } else {
                $memberAddressDomisili->update($dataDom);
            }

            return redirect()->route('membership.biodata.education');
        } catch (\Exception $e) {
            return redirect(route('membership.biodata.address'))
				->with('error', trans('message.error_save'));
        }
    }

    public function wizardEmergency()
    {
        $params['pageTitle']    = 'Pengisian Biodata';
        $params['data']         = Members::findOrFail(Auth::user()->id);
        $params['state']        = 'kontak-darurat';
        $params['provinsi']     = Provinsi::getProvinsiDropdownList(true);
        return view('membership::members.biodata.wizard-3', $params);
    }

    public function wizardEmergencySave(Request $request)
    {
        try {
            $memberID       = Auth::user()->id;
            
            $data['type']   = 'emr';
            $data['name']   = $request->name;
            $data['phone']  = $request->phone;
            $data['line1']  = $request->line1;
            $data['line2']  = $request->line2;
            $data['rt']     = $request->rt;
            $data['rw']     = $request->rw;
            $data['blok']   = $request->blok;
            $data['no']     = $request->nomor;
            $data['kel_id'] = $request->kelurahan;
            $data['kec_id'] = $request->kecamatan;
            $data['kab_id'] = $request->kabupaten;
            $data['prov_id']= $request->provinsi;
            $data['zipcode']= $request->kodepos;
            $data['member_id']  = $memberID;

            $memberAddressKtp = MemberAddress::where('member_id', $memberID)
                ->where('type', 'emr');

            if($memberAddressKtp->count() == 0) {
                MemberAddress::create($data);
            } else {
                $memberAddressKtp->update($data);
            }

            return redirect()->route('membership.biodata.education');
        } catch (\Exception $e) {
            return redirect(route('membership.biodata.emergency'))
				->with('error', trans('message.error_save'));
        }
    }

    public function wizardEducation()
    {
        $params['pageTitle']    = 'Pengisian Biodata';
        $params['state']        = 'pendidikan';
        $params['data']         = Members::findOrFail(Auth::user()->id);
        $params['degree']       = MemberEducation::getDegree();
        return view('membership::members.biodata.wizard-4', $params);
    }

    public function wizardEducationSave(Request $request)
    {
        try {
            $memberID       = Auth::user()->id;
            
            $data['degree']     = $request->degree;
            $data['institution']= uppercase($request->institution);
            $data['majors']     = uppercase($request->majors);
            // $data['start_year'] = $request->start_year;
            $data['graduation'] = $request->graduation;
            // $data['grade']      = $request->grade;
            // $data['kab_id']     = $request->kabupaten;
            // $data['prov_id']    = $request->provinsi;
            // $data['note']       = $request->note;
            $data['member_id']  = $memberID;

            $memberEducation = MemberEducation::where('member_id', $memberID);

            if($memberEducation->count() == 0) {
                MemberEducation::create($data);
            } else {
                $memberEducation->update($data);
            }

            return redirect()->route('membership.biodata.experience');
        } catch (\Exception $e) {
            return redirect(route('membership.biodata.education'))
				->with('error', trans('message.error_save'));
        }
    }

    public function wizardExperience()
    {
        // setlocale(LC_TIME, 'id_ID');
        $params['pageTitle']    = 'Pengisian Biodata';
        $params['state']        = 'experience';
        $params['data']         = Members::findOrFail(Auth::user()->id);
        $params['workYear']     = MemberExperience::getWorkYear();
        $params['workMonth']    = MemberExperience::getWorkMonth();
        return view('membership::members.biodata.wizard-5', $params);
    }

    public function wizardExperienceSave(Request $request)
    {
        try {
            $memberID       = Auth::user()->id;
            
            $data['company']    = uppercase($request->company);
            $data['position']   = uppercase($request->position);
            $data['work_year']  = $request->work_year;
            $data['work_month'] = $request->work_month;
            // $data['summary']    = $request->summary;
            // $data['year_start'] = $request->year_start;
            // $data['month_start']= $request->month_start;
            // $data['year_end']   = $request->year_end;
            // $data['month_end']  = $request->month_end;
            // $data['is_present'] = isset($request->is_present) ? 1 : 0;
            // $data['salary']     = cleanNominal($request->salary);
            // $data['kab_id']     = $request->kabupaten;
            // $data['prov_id']    = $request->provinsi;
            $data['member_id']  = $memberID;

            $memberExperience = MemberExperience::where('member_id', $memberID);

            if($memberExperience->count() == 0) {
                MemberExperience::create($data);
            } else {
                $memberExperience->update($data);
            }

            return redirect()->route('membership.biodata.document');
        } catch (\Exception $e) {
            return redirect(route('membership.biodata.experience'))
				->with('error', trans('message.error_save'));
        }
    }

    public function wizardDocument()
    {
        $params['pageTitle']    = 'Pengisian Biodata';
        $params['state']        = 'document';
        $params['data']         = Members::findOrFail(Auth::user()->id);
        return view('membership::members.biodata.wizard-6', $params);
    }

    public function wizardDocumentSave(Request $request)
    {
        try {

            if($request->hasFile('foto')) {
                ini_set('memory_limit','256M');
                $fileExt    = strtolower($request->file('foto')->getClientOriginalExtension());
                $fileName   = Uuid::uuid4() . '.'  . $fileExt;
                $data['attc_foto']    = $fileName;
                $thumbnailName = 'thumb_' . $fileName;
                /* Resize File */
                $this->createThumbnail($request->file('foto'), 800, 800, $fileName);
                /* create Thumbnail */
                $this->createThumbnail($request->file('foto'), 300, 300, $thumbnailName);
            }
            if($request->hasFile('ktp')) {
                ini_set('memory_limit','256M');
                $fileExt    = strtolower($request->file('ktp')->getClientOriginalExtension());
                $fileName   = Uuid::uuid4() . '.'  . $fileExt;
                $data['attc_card']    = $fileName;
                $thumbnailName = 'thumb_' . $fileName;
                /* Resize File */
                $this->createThumbnail($request->file('ktp'), 800, 800, $fileName);
                /* create Thumbnail */
                $this->createThumbnail($request->file('ktp'), 300, 300, $thumbnailName);
            }
            // if($request->hasFile('kk')) {
            //     ini_set('memory_limit','256M');
            //     $fileExt    = strtolower($request->file('kk')->getClientOriginalExtension());
            //     $fileName   = Uuid::uuid4() . '.'  . $fileExt;
            //     $data['attc_kk']    = $fileName;
            //     $thumbnailName = 'thumb_' . $fileName;
            //     /* Resize File */
            //     $this->createThumbnail($request->file('kk'), 800, 800, $fileName);
            //     /* create Thumbnail */
            //     $this->createThumbnail($request->file('kk'), 300, 300, $thumbnailName);
            // }
            // if($request->hasFile('npwp')) {
            //     ini_set('memory_limit','256M');
            //     $fileExt    = strtolower($request->file('npwp')->getClientOriginalExtension());
            //     $fileName   = Uuid::uuid4() . '.'  . $fileExt;
            //     $data['attc_npwp']    = $fileName;
            //     $thumbnailName = 'thumb_' . $fileName;
            //     /* Resize File */
            //     $this->createThumbnail($request->file('npwp'), 800, 800, $fileName);
            //     /* create Thumbnail */
            //     $this->createThumbnail($request->file('npwp'), 300, 300, $thumbnailName);
            // }

            $member = MemberData::where('member_id', Auth::user()->id)->firstOrFail();
            $member->update($data);
            $member->getMember->update([
                'doc_status' => Members::DOC_STATUS_PENDING
            ]);

            DocumentApproval::create([
                'doc_status' => Members::DOC_STATUS_PENDING,
                'member_id' => Auth::user()->id
            ]);

            return redirect(route('membership.home'));
        } catch (\Exception $e) {
            return redirect(route('membership.biodata.document'))
                ->with('error', trans('message.error_save'));
        }
    }

    private function createThumbnail($path, $width, $height, $newFilename)
    {
        $img = Image::make($path)->resize($width, $height, function($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        Storage::disk($this->disk)->put(
            'membership/' . Auth::user()->id . '/' . $newFilename,
            $img->stream(),
            'public'
        );
    }
    
}
