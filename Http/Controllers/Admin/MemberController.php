<?php

namespace Modules\Membership\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Yajra\DataTables\Facades\DataTables;
use Modules\Membership\Entities\Members;
use Modules\Membership\Entities\View\MemberAvailableView;

class MemberController extends Controller
{
    protected $pageTitle;

    public function __construct()
    {
        $this->pageTitle = 'Members';
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('membership::admin.members.index', $params);
    }

    public function indexData(Request $request)
    {
        if($request->ajax()) {
            $data = new MemberAvailableView();
            if(isset($request->start_date)) {
                $data = $data->whereDate('created_at', '>=', dateFormatYmd($request->start_date));
            } else {
                $data = $data->whereDate('created_at', '>=', Carbon::now()->subDays(1));
            }
            if(isset($request->end_date)) {
                $data = $data->whereDate('created_at', '<=', dateFormatYmd($request->end_date));
            } else {
                $data = $data->whereDate('created_at', '<=', Carbon::now());
            }
            $data = $data->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('checkboxes', function($row) {
                    return null;
                })
                ->addColumn('action', function($row) {
                    $actionBtn = 
                    '
                    <div class="btn-group">
                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Action</span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li>
                                <a href="#" onclick="getDetail(\''.$row->id.'\');return false;"><i class="fa fa-eye"></i>Detail</a>
                            </li>
                        </ul>
                    </div>
                    ';
                    return $actionBtn;
                })
                ->rawColumns([
                    'action',
                ])
                ->make(true);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request)
    {
        $id = $request->id;
        $params['data']         = Members::findOrFail($id);
        return view('membership::admin.members.show', $params);
    }

    public function bulkDelete(Request $request)
    {
        try {
            $ids = $request->id;
            foreach ($ids as $id) {
                Members::findOrFail($id)->delete();
            }
            return response()->json([
                'message' => 'Data berhasil dihapus.'
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e
            ]);
        }
    }
}
