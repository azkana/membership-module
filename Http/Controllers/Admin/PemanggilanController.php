<?php

namespace Modules\Membership\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Logs\Email;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use Modules\Membership\Entities\Members;
use Modules\Membership\Entities\Pemanggilan;
use Modules\Membership\Jobs\MembershipPemanggilanJob;
use Modules\Membership\Entities\View\MemberPemanggilanView;

class PemanggilanController extends Controller
{
    protected $pageTitle;

    public function __construct()
    {
        $this->pageTitle = 'Pemanggilan';
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('membership::admin.members.pemanggilan', $params);
    }

    public function indexData(Request $request)
    {
        if($request->ajax()) {
            $data = new MemberPemanggilanView();
            if(isset($request->start_date)) {
                $data = $data->whereDate('created_at', '>=', dateFormatYmd($request->start_date));
            } else {
                $data = $data->whereDate('created_at', '>=', Carbon::now()->subDays(1));
            }
            if(isset($request->end_date)) {
                $data = $data->whereDate('created_at', '<=', dateFormatYmd($request->end_date));
            } else {
                $data = $data->whereDate('created_at', '<=', Carbon::now());
            }
            $data = $data->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('checkboxes', function($row) {
                    return null;
                })
                ->addColumn('action', function($row) {
                    $actionBtn = 
                    '
                    <div class="btn-group">
                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Action</span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li>
                                <a href="#" onclick="getDetail(\''.$row->id.'\');return false;"><i class="fa fa-eye"></i>Detail</a>
                            </li>
                        </ul>
                    </div>
                    ';
                    return $actionBtn;
                })
                ->rawColumns([
                    'action',
                ])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function prosesBulk(Request $request)
    {
        try {
            $ids = $request->id;
            foreach ($ids as $id) {
                $data['notif_sent'] = true;
                $data['sent_at']    = Carbon::now()->format('Y-m-d H:i:s');
                $data['expired_at'] = Carbon::now()->addDays(1)->format('Y-m-d H:i:s');
                $data['status']     = Pemanggilan::STATUS_SENT;
                $data['member_id']  = $id;
                $data['created_by'] = Auth::user()->id;
                $data['updated_by'] = Auth::user()->id;
                Pemanggilan::create($data);

                $member = Members::findOrFail($id);
                $member->update(['rec_status' => Members::REC_STATUS_PEMANGGILAN]);

                $dataEmail['full_name'] = $member->full_name;
                $dataEmail['email']     = $member->email;

                $logEmail = Email::create([
                    'email_from'    => config('mail.from.address'),
                    'email_to'      => $member['email'],
                    'email_subject' => 'Jobsdku - Konfirmasi Pemanggilan',
                    'email_status'  => 'Q',
                    'module'        => 'membership',
                    'queue_at'      => Carbon::now()->format('Y-m-d H:i:s')
                ]);

                $dataEmail['log_id']  = $logEmail->id;

                MembershipPemanggilanJob::dispatch($dataEmail)
                    ->delay(now()->addMinutes(1));

            }
            return response()->json([
                'message' => 'Data berhasil diproses.'
            ]); 
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
    }

}
