<?php

namespace Modules\Membership\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Membership\Entities\View\MemberMcuView;
use Yajra\DataTables\Facades\DataTables;

class McuController extends Controller
{
    protected $pageTitle;

    public function __construct()
    {
        $this->pageTitle = 'Medical Check-up';
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('membership::admin.members.mcu', $params);
    }

    public function indexData(Request $request)
    {
        if($request->ajax()) {
            $data = new MemberMcuView();
            if(isset($request->start_date)) {
                $data = $data->whereDate('created_at', '>=', dateFormatYmd($request->start_date));
            } else {
                $data = $data->whereDate('created_at', '>=', Carbon::now()->subDays(1));
            }
            if(isset($request->end_date)) {
                $data = $data->whereDate('created_at', '<=', dateFormatYmd($request->end_date));
            } else {
                $data = $data->whereDate('created_at', '<=', Carbon::now());
            }
            $data = $data->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('checkboxes', function($row) {
                    return null;
                })
                ->addColumn('action', function($row) {
                    $actionBtn = 
                    '
                    <div class="btn-group">
                        <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                            <span class="caret"></span>
                            <span class="sr-only">Action</span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li>
                                <a href="#" onclick="getDetail(\''.$row->id.'\');return false;"><i class="fa fa-eye"></i>Detail</a>
                            </li>
                        </ul>
                    </div>
                    ';
                    return $actionBtn;
                })
                ->rawColumns([
                    'action',
                ])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('membership::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('membership::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('membership::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
