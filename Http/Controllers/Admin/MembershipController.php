<?php

namespace Modules\Membership\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class MembershipController extends Controller
{
    protected $pageTitle;

    public function __construct()
    {
        $this->pageTitle = 'Membership Dashboard';
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $params['pageTitle']    = $this->pageTitle;
        return view('membership::index', $params);
    }

}
