<?php

namespace Modules\Membership\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MembershipPaymentSuccess extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $params['css']          = config('beautymail.view.css');
        $params['logo']         = config('beautymail.view.logo');
        $params['senderName']   = config('beautymail.view.senderName');
        $params['unsubscribe']  = null;
        $params['data']         = $this->data;
        return $this->view('membership::admin.emails.payment-success', $params)
            ->subject('Jobsdku - Pembayaran Berhasil diverifikasi');
    }
}
