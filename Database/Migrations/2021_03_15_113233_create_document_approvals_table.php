<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_document_approval', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('doc_status', 20)->nullable();
            $table->string('note', 200)->nullable();
            $table->unsignedInteger('processed_by')->nullable();
            $table->dateTime('processed_at')->nullable();
            $table->uuid('member_id');

            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('mbr_members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_document_approval');
    }
}
