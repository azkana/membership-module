<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemanggilansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_pemanggilan', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->boolean('notif_sent')->default(0);
            $table->dateTime('sent_at')->nullable();
            $table->dateTime('expired_at')->nullable();
            $table->dateTime('read_at')->nullable();
            $table->dateTime('confirmed_at')->nullable();
            $table->dateTime('rejected_at')->nullable();
            $table->string('status', 20)->nullable();
            $table->text('note', 200)->nullable();

            $table->uuid('member_id');
            $table->integer('created_by')->comment('Dibuat Oleh');
            $table->integer('updated_by')->comment('Diubah Oleh');
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('mbr_members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_pemanggilan');
    }
}
