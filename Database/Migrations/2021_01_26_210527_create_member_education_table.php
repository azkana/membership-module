<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_education', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('degree', 10)->nullable();
            $table->string('institution', 50)->nullable();
            $table->string('majors', 50)->nullable();
            $table->string('start_year', 4)->nullable();
            $table->string('graduation', 4)->nullable();
            $table->boolean('on_progress')->default(0);
            $table->float('grade')->nullable();
            $table->string('kab_id', 4)->nullable();
            $table->string('prov_id', 2)->nullable();
            $table->string('note', 100)->nullable();

            $table->uuid('member_id');
            $table->integer('created_by')->nullable()->comment('Dibuat Oleh');
            $table->integer('updated_by')->nullable()->comment('Diubah Oleh');
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('mbr_members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_education');
    }
}
