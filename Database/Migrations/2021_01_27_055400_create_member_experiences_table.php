<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_experiences', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('company', 100)->nullable();
            $table->string('position', 50)->nullable();
            $table->integer('work_year')->default(0)->nullable();
            $table->integer('work_month')->default(0)->nullable();
            $table->text('summary')->nullable();
            $table->string('year_start', 4)->nullable();
            $table->string('month_start', 2)->nullable();
            $table->string('year_end', 4)->nullable();
            $table->string('month_end', 2)->nullable();
            $table->boolean('is_present')->default(0);
            $table->double('salary')->default(0);
            $table->string('kab_id', 4)->nullable();
            $table->string('prov_id', 2)->nullable();

            $table->uuid('member_id');
            $table->integer('created_by')->nullable()->comment('Dibuat Oleh');
            $table->integer('updated_by')->nullable()->comment('Diubah Oleh');
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('mbr_members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_experiences');
    }
}
