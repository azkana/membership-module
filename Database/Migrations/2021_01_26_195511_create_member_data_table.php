<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_data', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('nik', 16)->nullable()->comment('No. KTP');
            $table->string('phone', 20)->nullable()->comment('No. HP');
            $table->string('wa', 20)->nullable()->comment('No. Whatsapp');
            $table->string('b_place', 10)->nullable()->comment('Tempat Lahir');
            $table->date('b_date')->nullable()->comment('Tgl. Lahir');
            $table->string('gender', 1)->nullable()->comment('Jenis Kelamin');
            $table->double('tb')->default(0)->comment('Tinggi Badan');
            $table->double('bb')->default(0)->comment('Berat Badan');
            $table->string('npwp', 20)->nullable();
            $table->string('kk', 20)->nullable();
            $table->unsignedInteger('religion_id')->nullable()->comment('Id Agama');
            $table->unsignedInteger('marital_id')->nullable()->comment('Id Status Perkawinan');
            $table->string('attc_card', 80)->nullable()->comment('File ID Card');
            $table->string('attc_foto', 80)->nullable()->comment('File Pas Foto');
            $table->string('attc_npwp', 80)->nullable()->comment('File NPWP');
            $table->string('attc_kk', 80)->nullable()->comment('File KK');
            $table->string('attc_cv', 80)->nullable()->comment('File CV');
            $table->string('nationality', 10)->nullable();
            $table->uuid('member_id');
            $table->unsignedInteger('created_by')->nullable()->comment('Dibuat Oleh');
            $table->unsignedInteger('updated_by')->nullable()->comment('Diubah Oleh');
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('mbr_members')->onDelete('cascade');
            $table->foreign('religion_id')->references('id')->on('m_agama');
            $table->foreign('marital_id')->references('id')->on('m_marital');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_data');
    }
}
