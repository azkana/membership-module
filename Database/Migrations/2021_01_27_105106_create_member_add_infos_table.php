<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberAddInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_add_info', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('mother', 50)->nullable();
            $table->string('father', 50)->nullable();
            $table->double('expected_salary')->default(0);
            $table->string('expected_location', 100)->nullable();
            $table->text('other_info', 3000)->nullable();
            $table->uuid('member_id');
            $table->integer('created_by')->comment('Dibuat Oleh');
            $table->integer('updated_by')->comment('Diubah Oleh');
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('mbr_members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_add_info');
    }
}
