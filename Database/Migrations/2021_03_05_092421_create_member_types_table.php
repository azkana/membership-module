<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_types', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('code', 10)->unique();
            $table->string('name', 100);
            $table->text('description', 500)->nullable();
            $table->double('amount')->default(0);
            $table->boolean('is_active')->default(1);
            $table->integer('created_by')->nullable()->comment('Dibuat Oleh');
            $table->integer('updated_by')->nullable()->comment('Diubah Oleh');
            $table->timestamps();

            $table->index('code');
            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_types');
    }
}
