<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_members', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('no_member', 10)->nullable()->comment('Nomor Member');
            $table->string('first_name', 60)->comment('First Name');
            $table->string('last_name', 60)->comment('Last Name');
            $table->string('email', 150)->unique()->comment('Email');
            $table->string('password')->comment('Password');
            $table->timestamp('verified_at')->nullable()->comment('Email Verified Date');
            $table->rememberToken();
            $table->string('membership', 10)->nullable();
            $table->string('doc_status', 10)->nullable();
            $table->string('rec_status', 5)->nullable();
            $table->boolean('is_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_members');
    }
}
