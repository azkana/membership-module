<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class CreateMemberPemanggilanViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW vw_mbr_pem
            AS
            SELECT 
                m.id, m.no_member, CONCAT(m.first_name, ' ', m.last_name) as full_name, m.email,
                md.nik, md.wa, md.gender, TIMESTAMPDIFF(YEAR, md.b_date, CURDATE()) as age,
                m.created_at, m.updated_at
            FROM mbr_members m
            LEFT JOIN mbr_data md ON m.id = md.member_id 
            WHERE m.rec_status = 'PEM'
            ORDER BY m.created_at DESC 
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
            DROP VIEW IF EXISTS vw_mbr_pem
        ");
    }
}
