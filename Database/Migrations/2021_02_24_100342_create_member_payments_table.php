<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_payments', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('number', 50)->unique();
            $table->double('amount')->default(0);
            $table->string('method', 20);
            $table->string('status', 20);
            $table->string('token', 200)->nullable();
            $table->json('payload')->nullable();
            $table->string('payment_type', 100)->nullable();
            $table->string('va_number', 100)->nullable();
            $table->string('vendor_name', 100)->nullable();
            $table->string('biller_code', 200)->nullable();
            $table->string('bill_key', 200)->nullable();
            $table->uuid('order_id');
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('mbr_orders')->onDelete('cascade');
            $table->index('number');
            $table->index('method');
            $table->index('token');
            $table->index('payment_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_payments');
    }
}
