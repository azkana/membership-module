<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_orders', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('code')->unique();
            $table->double('amount')->default(0);
            $table->double('amount_uniq')->default(0);
            $table->double('discount_percent')->default(0);
            $table->double('discount_amount')->default(0);
            $table->double('grand_total')->default(0);
            $table->dateTime('order_date')->nullable();
            $table->string('status', 30)->nullable();
            $table->dateTime('due_date')->nullable();
            $table->string('payment_status', 30)->nullable();
            $table->string('payment_token')->nullable();
            $table->string('payment_url')->nullable();
            $table->string('note')->nullable();
            $table->uuid('member_id');
            $table->unsignedInteger('approved_by')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('mbr_members')->onDelete('cascade');
            $table->foreign('approved_by')->references('id')->on('users');
            $table->index('code');
            $table->index(['code', 'order_date']);
            $table->index('payment_token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_orders');
    }
}
