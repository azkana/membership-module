<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_address', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('type', 30)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('line1', 100)->nullable();
            $table->string('line2', 100)->nullable();
            $table->string('rt', 3)->nullable();
            $table->string('rw', 3)->nullable();
            $table->string('blok', 20)->nullable();
            $table->string('no',10)->nullable();
            $table->string('kel_id', 10)->nullable();
            $table->string('kec_id', 6)->nullable();
            $table->string('kab_id', 4)->nullable();
            $table->string('prov_id', 2)->nullable();
            $table->string('zipcode', 5)->nullable();
            $table->string('phone', 20)->nullable();
            $table->uuid('member_id');
            $table->integer('created_by')->nullable()->comment('Dibuat Oleh');
            $table->integer('updated_by')->nullable()->comment('Diubah Oleh');
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('mbr_members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_address');
    }
}
