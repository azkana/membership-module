<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberLanguagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mbr_languages', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name', 20)->nullable();
            $table->integer('spoken')->default(0);
            $table->integer('written')->default(0);
            $table->uuid('member_id');
            $table->unsignedInteger('created_by')->comment('Dibuat Oleh');
            $table->unsignedInteger('updated_by')->comment('Diubah Oleh');
            $table->timestamps();

            $table->foreign('member_id')->references('id')->on('mbr_members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mbr_languages');
    }
}
