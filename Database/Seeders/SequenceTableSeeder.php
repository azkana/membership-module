<?php

namespace Modules\Membership\Database\Seeders;

use App\Models\Configs\Sequence;
use Illuminate\Database\Seeder;

class SequenceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $sequence = [
            [
                'module_id' => 'membership',
                'menu_id' => 'member_order',
                'period' => date('m')
            ],
            [
                'module_id' => 'membership',
                'menu_id' => 'member_payment',
                'period' => date('m')
            ],
            [
                'module_id' => 'membership',
                'menu_id' => 'member_number',
                'period' => date('y')
            ],
        ];

        foreach ($sequence as $row) {
            $seqCheck = Sequence::where('module_id', $row['module_id'])
                ->where('menu_id', $row['menu_id'])
                ->count();
            
            if($seqCheck > 0) {
                $this->command->info('Sequence module ' . $row['module_id'] . ' menu ' . $row['menu_id'] . ' was exists.');
            } else {
                Sequence::create([
                    'module_id' => $row['module_id'],
                    'menu_id' => $row['menu_id'],
                    'period' => $row['period'],
                    'last_value' => 0
                ]);
                $this->command->info('Sequence module ' . $row['module_id'] . ' menu ' . $row['menu_id'] . ' created successfully.');
            }
        }

    }
}
