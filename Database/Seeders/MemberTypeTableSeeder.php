<?php

namespace Modules\Membership\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Membership\Entities\MemberType;

class MemberTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'code' => 'FREE',
                'name' => 'Free Member',
                'description' => '',
                'amount' => 0
            ],
            [
                'code' => 'PRO',
                'name' => 'Pro Member',
                'description' => '',
                'amount' => 50000
            ],
        ];

        foreach ($data as $row) {
            $memberType = MemberType::where('code', $row['code'])->count();
            if($memberType > 0) {
                $this->command->info('Member Type ' . $row['code'] . ' was exists.');
            } else {
                MemberType::create([
                    'code' => $row['code'],
                    'name' => $row['name'],
                    'description' => $row['description'],
                    'amount' => $row['amount'],
                    'is_active' => true,
                    'created_by' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_by' => 1,
                    'updated_at' => date('Y-m-d H:i:s')
                ]);
                $this->command->info('Member Type ' . $row['code'] . ' created successfully');
            }
        }
    }
}
