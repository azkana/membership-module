<?php

namespace Modules\Membership\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MembershipDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(MembersTableSeeder::class);
        $this->call(MemberTypeTableSeeder::class);
        $this->call(SequenceTableSeeder::class);
    }
}
