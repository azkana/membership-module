<?php

namespace Modules\Membership\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Membership\Entities\Members;
use Ramsey\Uuid\Uuid;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $members = [
            [ 
                'id' => Uuid::uuid4(),
                'first_name' => 'John',
                'last_name' => 'Doe',
                'email' => 'shinoby11@gmail.com',
                'password' => bcrypt('secret'),
                'verified_at' => date('Y-m-d H:i:s')
            ]
        ];

        foreach ($members as $row) {
            $member = Members::where('email', $row['email'])->first();
            if(isset($member)) {
                $this->command->info('Member ' . $row['email'] . ' was exists.');
            } else {
                Members::create([
                    'first_name' => $row['first_name'],
                    'last_name' => $row['last_name'],
                    'email' => $row['email'],
                    'password' => $row['password'],
                    'verified_at' => $row['verified_at'],
                    'rec_status' => 'AVA'
                ]);
                $this->command->info('Member ' . $row['email'] . ' created successfully');
            }
        }
    }
}
