<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('membership')->group(function() {
//     Route::get('/', 'Admin\MembershipController@index');
// });

Route::middleware(['auth'])->group(function () {

    Route::prefix('manage')->group(function() {

        Route::prefix('members')->group(function() {

            Route::name('membership.')->group(function() {

                Route::get('dashboard', 'Admin\MembershipController@index')->name('dashboard');

                Route::get('index', 'Admin\MemberController@index')->name('index');
                Route::get('index/data', 'Admin\MemberController@indexData')->name('index.data');
                Route::get('detail', 'Admin\MemberController@show')->name('show');
                Route::delete('delete', 'Admin\MemberController@bulkDelete')->name('delete.bulk');

                /** Pemanggilan */
                Route::get('pemanggilan', 'Admin\PemanggilanController@index')->name('pemanggilan');
                Route::get('pemanggilan/data', 'Admin\PemanggilanController@indexData')->name('pemanggilan.data');
                Route::post('pemanggilan/proses-bulk', 'Admin\PemanggilanController@prosesBulk')->name('pemanggilan.proses.bulk');

                /** Konfirmasi Pemanggilan */
                Route::get('pemanggilan-konfirmasi', 'Admin\PemanggilanKonfirmasiController@index')->name('pemanggilan.konfirmasi');
                Route::get('pemanggilan-konfirmasi/data', 'Admin\PemanggilanKonfirmasiController@indexData')->name('pemanggilan.konfirmasi.data');

                /** Psikotest */
                Route::get('psikotest', 'Admin\PsikotestController@index')->name('psikotest');
                Route::get('psikotest/data', 'Admin\PsikotestController@indexData')->name('psikotest.data');

                /** Psikotest */
                Route::get('interview', 'Admin\InterviewController@index')->name('interview');
                Route::get('interview/data', 'Admin\InterviewController@indexData')->name('interview.data');

                /** Medical Check Up */
                Route::get('mcu', 'Admin\McuController@index')->name('mcu');
                Route::get('mcu/data', 'Admin\McuController@indexData')->name('mcu.data');

                /** Joined */
                Route::get('joined', 'Admin\JoinedController@index')->name('joined');
                Route::get('joined/data', 'Admin\JoinedController@indexData')->name('joined.data');

                /** Reports */
                Route::get('reports', 'Admin\ReportController@index')->name('report');

            });

        });

    });

});
Route::get('members/login', 'Members\Auth\LoginController@showLoginForm')->name('membership.login');
Route::post('members/login', 'Members\Auth\LoginController@login')->name('membership.login.submit');
Route::post('members/logout', 'Members\Auth\LoginController@logout')->name('membership.logout');
Route::post('members/register', 'Members\Auth\RegisterController@register')->name('membership.register.submit');
Route::get('members/register/success', 'Members\Auth\RegisterController@registerSuccess')->name('membership.register.success');
Route::get('members/email/verify/{id}', 'Members\Auth\RegisterController@verifyEmail')->name('membership.email.verify');
Route::get('members/email/verify/success/{id}/{status}', 'Members\Auth\RegisterController@verifyEmailSuccess')->name('membership.email.verify.success');

Route::post('members/payment/notification', 'Members\PaymentController@notification')->name('payment.notification');
Route::get('members/payment/completed', 'Members\PaymentController@completed')->name('payment.completed');
Route::get('members/payment/failed', 'Members\PaymentController@failed')->name('payment.failed');
Route::get('members/payment/unfinished', 'Members\PaymentController@unfinished')->name('payment.unfinished');

Route::middleware(['auth:member'])->group(function () {

    Route::prefix('members')->group(function() {

        Route::name('membership.')->group(function() {
            
            Route::get('/', 'Members\HomeController@membersIndex');
            Route::get('home', 'Members\HomeController@index')->name('home');
            Route::get('profile', 'Members\HomeController@profile')->name('profile');
            Route::get('account', 'Members\HomeController@account')->name('account');
            Route::get('personal', 'Members\HomeController@personal')->name('personal');
            Route::get('education', 'Members\HomeController@education')->name('education');
            Route::get('experiences', 'Members\HomeController@experiences')->name('experiences');
            Route::get('address', 'Members\HomeController@address')->name('address');

            /** Wizard Biodata */
            Route::get('biodata/check', 'Members\HomeController@biodatacheck')->name('biodata.check');
            Route::get('biodata/wizard', 'Members\HomeController@biodataWizard')->name('biodata.wizard');
            Route::get('biodata/personal', 'Members\HomeController@wizardPersonal')->name('biodata.personal');
            Route::post('biodata/personal', 'Members\HomeController@wizardPersonalSave')->name('biodata.personal.save');
            Route::get('biodata/address', 'Members\HomeController@wizardAddress')->name('biodata.address');
            Route::post('biodata/address', 'Members\HomeController@wizardAddressSave')->name('biodata.address.save');
            Route::get('biodata/emergency', 'Members\HomeController@wizardEmergency')->name('biodata.emergency');
            Route::post('biodata/emergency', 'Members\HomeController@wizardEmergencySave')->name('biodata.emergency.save');
            Route::get('biodata/education', 'Members\HomeController@wizardEducation')->name('biodata.education');
            Route::post('biodata/education', 'Members\HomeController@wizardEducationSave')->name('biodata.education.save');
            Route::get('biodata/experience', 'Members\HomeController@wizardExperience')->name('biodata.experience');
            Route::post('biodata/experience', 'Members\HomeController@wizardExperienceSave')->name('biodata.experience.save');
            Route::get('biodata/document', 'Members\HomeController@wizardDocument')->name('biodata.document');
            Route::post('biodata/document', 'Members\HomeController@wizardDocumentSave')->name('biodata.document.save');

            /* Upload File */
            Route::post('biodata/upload', 'Members\HomeController@upload')->name('biodata.upload');

            /* Recruitment */
            Route::get('recruitment', 'Members\RecruitmentController@index')->name('recruitment');
            Route::get('recruitment/pemanggilan', 'Members\RecruitmentController@pemanggilan')->name('rec.pemanggilan');
            Route::post('recruitment/pemanggilan/konfirmasi', 'Members\RecruitmentController@pemanggilanConfirm')->name('rec.pemanggilan.konfirmasi');
            Route::post('recruitment/pemanggilan/reject', 'Members\RecruitmentController@pemanggilanReject')->name('rec.pemanggilan.reject');

            /** Payment */
            Route::get('payment/order', 'Members\PaymentController@order')->name('payment.order');
            Route::post('payment/order/save', 'Members\PaymentController@orderSave')->name('payment.order.save');

            /* Test */
            // Route::get('payment/test', 'Members\PaymentController@testSendEmail')->name('payment.test');

        });
    });
});
